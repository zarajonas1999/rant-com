# Rant.com

The Website
+ [Web link](https://josh-edgar.gitlab.io/rant)

The Team
+ Johanson Bombaes | [linkedin](https://www.linkedin.com/in/johanson-bombaes/)
+ Chase Fienhold | [linkedin](http://www.linkedin.com/in/chase-fienhold-43a395299)
+ Edgar Mendoza | [linkedin](https://www.linkedin.com/in/mendozaedgar02/)
+ Jonas Zara | [linkedin](https://www.linkedin.com/in/jonas-cbz/)


# What is Rant.com?
"Rant.com" is a premiere social platform designed for users to communicate ideas and interests in a space that is a homage to other services such as Reddit or retro Youtube. Rant.com is intended to be a tongue and cheek service to anyone wanting to express their opinions on a selection of topics.

# Project Initialization
+ In your terminal of choice navigate into the directory you want to clone the project in.
+ Clone the repository typing: <code>https://gitlab.com/josh-edgar/rant</code>
+ Change directory to <code>rant</code>
+ Run <code>docker volume create rants</code>
+ Run <code>docker-compose build</code>
+ Run <code>docker-compose up</code>
+ Three containers should be up and running on your local Docker Desktop app. Fastapi, ghi-1, and db-1
+ To view the backend functionality go to <code>http://localhost:8000/docs</code>
+ To view the frontend functionality go to <code>http://localhost:5173</code>

# Design
## Wireframe
![Screenshot of wire frame of Rant.com's wireframe model](images/imagewire.png)

## Functionality through React Routes
##  [Home Page](http://localhost:5173)
+  Displays list of rants and has nav bar.

![Rant home page](images/imageHP.png)

## [Login/Logout](http://localhost:5173/user)
+ By a redirect users and login and logout of their account.

![Login screen](images/imageLogIn.png)

## [Create Account](http://localhost:5173/user)
+ Create a user account.

![Sign up page](images/image-SignUp.png)

## [Rant Detail](http://localhost:5173/rants/1)
+  Specific Rant is shown.

![Rant detail](images/imageRantDetail.png)

## [Rant Creation](http://localhost:5173/rants/new)
+ Rant create page.

![Rant creation](images/imageRantCreate.png)

## [Rant Category](http://localhost:5173/category/1)
+ View rants by category.

![Rants by category](images/imageCategory.png)

## [Comment creation](http://localhost:5173/rants/1)
+ Comment creation.

![Comment creation](images/imageCommentCreate.png)

## [ Rant List ](http://localhost:5173/rants)
+ List to display all rants.

![Rant list](images/imageRantList.png)

# FastAPI Endpoints
List of all FastAPI Endpoints for Rant.com when a 200 status code is returned. Check the Endpoints at http://localhost:8000/docs

Log in
+ GET/token -  Get token
+ POST/token -  Login
+ DELETE/token -  Logout

Users
+ GET/user -  Get all users
+ GET/user/{user_id} -  Get one user
+ GET/user/{username}/rants - Get rants by user
+ GET/user/{username}/comments - Get comments by user
+ POST/user/new - Create user
+ PUT/user/{user_id} - Update user info
+ DELETE/user/{user_id} -  Delete user

Rants
+ GET/rants - Get all rants
+ GET/rants/{rants_id} - Get one rant
+ PUT/rants/{rants_id} - Update rant
+ POST/rants/new - Create rant

Comments
+ GET/comments - Get all comments
+ GET/comments/{comments_id} - Get one comment
+ GET/comments/rant/{rant_detail_id} - Get all comments from specific rant
+ POST/comments - Create comment
+ PUT/comments/{comments_id} - Update comment
+ DELETE/comments/{comments_id} - Delete comment

Category
+ GET/category/{category_id} - Get one category

Likes
+ GET/likes - Get rant likes
+ GET/comment_likes - Get comment likes
+ POST/likes - Like rant
+ POST/comment_likes - Like comment

Dislikes
+ GET/dislikes -  Get dislike rant
+ GET/comment_dislikes - Get comment dislikes
+ POST/dislikes - Dislike rant
+ POST/comment_dislikes - Dislike comment
