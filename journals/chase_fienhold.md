In the journals, every day that you work on the project, you must make an entry in your journal after you've finished that day.
At a minimum, you'll need to include the following information in each entry:

--The date of the entry
--A list of features/issues that you worked on and who you worked with, if applicable
--A reflection on any design conversations that you had
--At least one ah-ha! moment that you had during your coding, however small

W14 Feb 26, 2024
Brainstormed project ideas with teammates. The conclusion is to make a Social media hubspace called Rant.com.
This site is going to be geared for tech professions and general user interaction.

Built the logo, wireframe, and a general representation on excalidraw.

Assisted in build MVP and Api Endpoints.

w14 Feb 29, 2024
Comments has a temp create endpoint and has a temp pool.
It will need rants and user to be able to create to the database.

W14 3/1/2024
Added routers and queries files and folders. Create endpoint for comments was created.

W15 3/4/2024
Finished Rants update endpoint. We conversed about how to add likes and dislikes to the app. We are thinking it might be optimal to have react update likes and dislikes
for comments and rants. An ah-ha today was debugging the update for rants. In the router it had an issue with update in the function not being called update_rants. updated

W16
3/11/2024
Built issues and assigned to the group. Attempted deployment, group decided we would hold off until we finished react components.
All commands are in notion at the moment under Mod 3.

3/12/2024
Finished routing the nav links for current pages available on main. Helped with t/s update rant front end.

W16 3/14/2024
over last two days built out category queries for get category and router for category. finished retrieving info for rant by category.
will need to implement clickable rants.

W17 3/18/2024
Built the nav links to go to all of the categories. Fixed bugs on CategoryRant.

W17 3/19/2024
Created unit test for rant creation.

W17 3/21/2024
Finished creating ReadMe.md. Next step learning tailwind and implementing it on my portions of the project.

Endpoints:
Rant PUT
Category GET detail
Comment POST

Test:
POST rant test

React:
CategoryRant.jsx
MainPage.jsx
Nav.jsx
App.jsx