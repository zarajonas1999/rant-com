import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.q_users import UserRepo, UserOutWithPassword


class UserAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        accounts: UserRepo,
    ):
        # Use your repo to get the account based on the
        # username (which could be an email)
        return accounts.get(username)

    def get_account_getter(
        self,
        accounts: UserRepo = Depends(),
    ):
        # Return the accounts. That's it.
        return accounts

    def get_hashed_password(self, account: UserOutWithPassword):
        # Return the encrypted password value from your
        # account object
        return account.__getitem__('password')

    def get_account_data_for_cookie(self, account: UserOutWithPassword):
        # Return the username and the data for the cookie.
        # You must return TWO values from this method.
        # print("ACCOUNT",account)
        # d = account['username']
        # print(d)
        return account["username"], account


authenticator = UserAuthenticator(os.environ["SIGNING_KEY"])
