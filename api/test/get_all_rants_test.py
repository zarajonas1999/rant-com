from queries.q_rants import RantsRepository
from fastapi.testclient import TestClient
from main import app

client = TestClient(app)


class EmptyRantQueries:
    def get_all(self):
        return []


# sanity check
def test_init():
    assert 1 != 0


# check get all rants
def test_rants_getall():
    # ARRANGE
    app.dependency_overrides[RantsRepository] = EmptyRantQueries
    # ACT
    response = client.get("/rants")
    app.dependency_overrides = {}
    # ASSERT
    assert response.status_code == 200
    assert response.json() == []
