from queries.q_users import UserRepo
from fastapi.testclient import TestClient
from main import app


client = TestClient(app)


class EmptyUserQueries:
    def get_all(self):
        return []


# sanity check
def test_init():
    assert 1 != 0


# check get user
def test_user_getall():
    # ARRANGE
    app.dependency_overrides[UserRepo] = EmptyUserQueries
    # ACT
    response = client.get("/user/")
    app.dependency_overrides = {}
    # ASSERT
    assert response.status_code == 200
    assert response.json() == []
