from queries.q_comments import CommentRepository
from fastapi.testclient import TestClient
from main import app


client = TestClient(app)


class EmptyCommentQueries:
    def get_all(self):
        return []


# sanity check
def test_init():
    assert 1 != 0


# check get_all comments
def test_comment_getall():
    # ARRANGE
    app.dependency_overrides[CommentRepository] = EmptyCommentQueries
    # ACT
    response = client.get("/comments/")
    app.dependency_overrides = {}
    # ASSERT
    assert response.status_code == 200
    assert response.json() == []
