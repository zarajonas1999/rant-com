from fastapi.testclient import TestClient
from main import app
from queries.q_rants import RantsRepository, RantsOut


client = TestClient(app)


class CreateRantQueries:
    def create(self, rant) -> RantsOut:
        result = {
            "id": "7",
            "title": "Tips for dinner out",
            "category_id": 1,
            "content": "Dinner for two is more expensive than dinner for one",
            "author_id": 2,
            "date_created": "2024-03-19",
        }
        result.update(rant.dict())
        return RantsOut(**result)


def test_create_rant():
    # ARRANGE
    app.dependency_overrides[RantsRepository] = CreateRantQueries
    rant = {
        "title": "Tips for dinner out",
        "category_id": 1,
        "content": "Dinner for two is more expensive than dinner for one",
        "author_id": 2,
        "date_created": "2024-03-19",
    }
    expected = {
        "id": "7",
        "title": "Tips for dinner out",
        "category_id": 1,
        "content": "Dinner for two is more expensive than dinner for one",
        "author_id": 2,
        "date_created": "2024-03-19",
    }
    # ACT
    response = client.post("/rants/new", json=rant)
    app.dependency_overrides = {}
    # ASSERT
    assert response.status_code == 200
    assert response.json() == expected
