steps = [
    [
        """
        CREATE TABLE categories (
            id SERIAL PRIMARY KEY NOT NULL UNIQUE,
            name VARCHAR(50) NOT NULL UNIQUE
        );
        """,
        """
        DROP TABLE categories;
        """
    ],
    [
        # Create the table
        """
        CREATE TABLE rants (
            id SERIAL PRIMARY KEY NOT NULL UNIQUE,
            title VARCHAR(50) NOT NULL,
            category_id INT NOT NULL,
            content TEXT NOT NULL,
            author_id INT NOT NULL,
            date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY (author_id) REFERENCES users(id),
            FOREIGN KEY (category_id) REFERENCES categories(id)
        );
        """,
        # Drop the table
        """
        DROP TABLE rants;
        """
    ],
    [
        # Create the table
        """
        CREATE TABLE rant_comments (
            id SERIAL PRIMARY KEY NOT NULL UNIQUE,
            content TEXT NOT NULL,
            author_id INT NOT NULL,
            rant_id INT NOT NULL,
            date_created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY (author_id) REFERENCES users(id),
            FOREIGN KEY (rant_id) REFERENCES rants(id)
        );
        """,
        # Drop the table
        """
        DROP TABLE rant_comments;
        """
    ],
    [
        """
        CREATE TABLE rant_likes (
            user_id INT NOT NULL
            , rant_id INT NOT NULL
            , PRIMARY KEY (user_id, rant_id)
            , FOREIGN KEY (user_id) REFERENCES users(id)
            , FOREIGN KEY (rant_id) REFERENCES rants(id)
        );
        """,
        # Drop the table
        """
        DROP TABLE rant_likes;
        """
    ],
    [
        """
        CREATE TABLE rant_dislikes (
            user_id INT NOT NULL
            , rant_id INT NOT NULL
            , PRIMARY KEY (user_id, rant_id)
            , FOREIGN KEY (user_id) REFERENCES users(id)
            , FOREIGN KEY (rant_id) REFERENCES rants(id)
        );
        """,
        # Drop the table
        """
        DROP TABLE rant_dislikes;
        """
    ],
    [
        """
        CREATE TABLE comment_likes (
            user_id INT NOT NULL
            , comment_id INT NOT NULL
            , PRIMARY KEY (user_id, comment_id)
            , FOREIGN KEY (user_id) REFERENCES users(id)
            , FOREIGN KEY (comment_id) REFERENCES rant_comments(id)
        );
        """,
        # Drop the table
        """
        DROP TABLE comment_likes;
        """
    ],
    [
        """
        CREATE TABLE comment_dislikes (
            user_id INT NOT NULL
            , comment_id INT NOT NULL
            , PRIMARY KEY (user_id, comment_id)
            , FOREIGN KEY (user_id) REFERENCES users(id)
            , FOREIGN KEY (comment_id) REFERENCES rant_comments(id)
        );
        """,
        # Drop the table
        """
        DROP TABLE comment_dislikes;
        """
    ]
]
