steps = [
    [
        """
        INSERT INTO categories VALUES
            (1, 'pets')
            , (2, 'software')
            , (3, 'hardware')
            , (4, 'movies')
            , (5, 'tv')
            , (6, 'music')
            , (7, 'products')
            , (8, 'news')
            , (9, 'shenanigans')
        ;
        """,
        """
        DELETE FROM categories
        WHERE id IN (1, 2, 3, 4, 5, 6, 7, 8, 9)
        ;
        """
    ]
]
