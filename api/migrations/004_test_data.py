steps = [
    [
        """
        INSERT INTO users VALUES
            (1, 'test', 'test'
            , '$2b$12$onE02cMi3kCM0xm.VTxjYO.K7BVfxFhsmJUjrPIbqMC8gKWIIoTXm')
            , (2, 'josh_elder', 'ranch@everything.com'
            , '$2b$12$onE02cMi3kCM0xm.VTxjYO.K7BVfxFhsmJUjrPIbqMC8gKWIIoTXm')
            , (3, 'edgar', 'edgar@rant.com'
            , '$2b$12$onE02cMi3kCM0xm.VTxjYO.K7BVfxFhsmJUjrPIbqMC8gKWIIoTXm')
            , (4, 'chase', 'chase@rant.com'
            , '$2b$12$onE02cMi3kCM0xm.VTxjYO.K7BVfxFhsmJUjrPIbqMC8gKWIIoTXm')
            , (5, 'jonas', 'jonas@rant.com'
            , '$2b$12$onE02cMi3kCM0xm.VTxjYO.K7BVfxFhsmJUjrPIbqMC8gKWIIoTXm')
            , (6, 'johanson', 'johanson@rant.com'
            , '$2b$12$onE02cMi3kCM0xm.VTxjYO.K7BVfxFhsmJUjrPIbqMC8gKWIIoTXm')
        ;
        """,
        """
        DELETE FROM users
        WHERE id IN (1, 2, 3, 4, 5, 6)
        ;
        """
    ],
    [
        """
        INSERT INTO rants (title, content, author_id, category_id)
        VALUES
            ('test', 'test', 1, 1)
            , ('ranch goes on everything', 'dont use MongoDB', 2, 2)
            , ('ranch goes on everything', 'dont use MongoDB', 2, 2)
            , ('ranch goes on everything', 'dont use MongoDB', 2, 2)
            , ('ranch goes on everything', 'dont use MongoDB', 2, 2)
            , ('ranch goes on everything', 'dont use MongoDB', 2, 2)
        ;
        """,
        """
        DELETE FROM rants
        WHERE id IN (1, 2, 3, 4, 5, 6)
        ;
        """
    ],
    [
        """
        INSERT INTO rant_comments (content, author_id, rant_id)
        VALUES
            ('test', 1, 1)
            , ('django is the devil', 2, 2)
            , ('django is the devil', 2, 2)
            , ('django is the devil', 2, 2)
            , ('django is the devil', 2, 2)
            , ('django is the devil', 2, 2)
        ;
        """,
        """
        DELETE FROM rants
        WHERE id IN (1, 2, 3, 4, 5, 6)
        ;
        """
    ]
]
