from pydantic import BaseModel
from typing import Union, Optional, List
from queries.pool import pool
from datetime import date


class Error(BaseModel):
    message: str


class DuplicateUserError(ValueError):
    message: str


class UserIn(BaseModel):
    username: str
    email: str
    password: str


class UserOut(BaseModel):
    id: int
    username: str
    email: str


class UserRantsOut(BaseModel):
    id: int
    title: str
    category_id: int
    content: str
    date_created: date
    username: str


class UserCommentsOut(BaseModel):
    id: int
    content: str
    rant_id: int
    date_created: date
    username: str


class UserOutWithPassword(UserOut):
    password: str


class UserRepo:
    def record_to_user_out(self, record) -> UserOutWithPassword:
        user_dict = {
            "id": record[0],
            "username": record[1],
            "email": record[2],
            "password": record[3]
        }
        return user_dict

    def create(self, user: UserIn, password: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO users (
                            username
                            , email
                            , password
                        )
                        VALUES
                            (%s, %s, %s)
                        RETURNING
                            id
                            , username
                            , email
                            , password;
                        """,
                        [
                            user.username,
                            user.email,
                            password,
                        ]
                    )
                    id = result.fetchone()[0]
                    return UserOutWithPassword(
                        id=id,
                        username=user.username,
                        email=user.email,
                        password=password,
                    )
        except Exception:
            return {"message": "Could not create a user"}

    def get(self, email: str) -> UserOutWithPassword:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            id
                            , username
                            , email
                            , password
                        FROM users
                        WHERE username = %s
                        """,
                        [email],
                    )
                    record = result.fetchone()
                    if record is None:
                        return None
                    return self.record_to_user_out(record)
        except Exception:
            return {"message": "Could not get user"}

    def get_all(self) -> Union[List[UserOutWithPassword], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id
                            , username
                            , email
                            , password
                        FROM users
                        ORDER BY id;
                        """
                    )
                    return [
                        self.record_to_user_out(record)
                        for record in db.fetchall()
                    ]
        except Exception as e:
            return Error(message="Error! " + str(e))

    def get_detail(self, user_id: int) -> Optional[UserOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            id
                            , username
                            , email
                            , password
                        FROM users
                        WHERE id = %s
                        """,
                        [user_id]
                    )
                    record = result.fetchone()
                    if record:
                        return self.record_to_user_out(record)
                    else:
                        return None
        except Exception as e:
            return Error(message="Error! " + str(e))

    def get_user_rants(self, username: str) -> List[UserRantsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            rants.id
                            , rants.title
                            , rants.category_id
                            , rants.content
                            , rants.date_created
                            , users.username
                        FROM rants
                        JOIN users ON rants.author_id = users.id
                        WHERE users.username = %s
                        """,
                        [username]
                    )
                    result = []
                    for record in db.fetchall():
                        rants = UserRantsOut(
                            id=record[0],
                            title=record[1],
                            category_id=record[2],
                            content=record[3],
                            date_created=record[4],
                            username=record[5],
                        )
                        result.append(rants)
                    return result
        except Exception as e:
            return Error(message="Error! " + str(e))

    def get_user_comments(self, username: str) -> List[UserCommentsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            rant_comments.id
                            , rant_comments.content
                            , rant_comments.rant_id
                            , rant_comments.date_created
                            , users.username
                        FROM rant_comments
                        JOIN users ON rant_comments.author_id = users.id
                        WHERE users.username = %s
                        """,
                        [username]
                    )
                    result = []
                    for record in db.fetchall():
                        rants = UserCommentsOut(
                            id=record[0],
                            content=record[1],
                            rant_id=record[2],
                            date_created=record[3],
                            username=record[4],
                        )
                        result.append(rants)
                    return result
        except Exception as e:
            return Error(message="Error! " + str(e))

    def update(self, user_id: int, user: UserIn) -> Union[UserOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE users
                        SET username = %s
                            , email = %s
                            , password = %s
                        WHERE id = %s
                        """,
                        [
                            user.username,
                            user.email,
                            user.password,
                            user_id
                        ]
                    )
                    updates = [
                        user_id,
                        user.username,
                        user.email,
                        user.password,
                    ]
                    return self.record_to_user_out(updates)
        except Exception as e:
            return Error(message="Error! " + str(e))

    def delete(self, user_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM users
                        WHERE id = %s
                        """,
                        [user_id]
                    )
                    return True
        except Exception as e:
            print(e)
            return False
