from pydantic import BaseModel
from typing import Optional, Union
from datetime import date
from queries.pool import pool


class Error(BaseModel):
    message: str


class CommentIn(BaseModel):
    content: str
    author_id: int
    rant_id: int
    date_created: date = date.today()


class CommentOut(BaseModel):
    id: int
    content: str
    author_id: int
    rant_id: int
    date_created: date = date.today()


class CommentRepository:

    def get_one(self, comment_id: int) -> Optional[CommentOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            id,
                            content,
                            author_id,
                            rant_id,
                            date_created
                        FROM rant_comments
                        WHERE id = %s;
                        """,
                        [comment_id],
                    )
                    record = (
                        result.fetchone()
                    )  # result = database, fetchone fetches one record
                    if record is None:
                        return None
                    comment = CommentOut(
                        id=record[0],
                        content=record[1],
                        author_id=record[2],
                        rant_id=record[3],
                        date_created=record[4],
                    )
                    return comment
        except Exception as e:
            print(e)
            return Error(message="Could not get comment")

    def delete(self, comment_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM rant_comments
                        WHERE
                            id = %s
                        """,
                        [comment_id],
                    )
                    return True
        except Exception as e:
            print(e)
            return False

    def update(
        self, comment_id: int, comment: CommentIn
    ) -> Union[CommentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE rant_comments
                        SET
                            content = %s,
                            author_id = %s,
                            rant_id = %s,
                            date_created = %s
                        WHERE id = %s
                        """,
                        [
                            comment.content,
                            comment.author_id,
                            comment.rant_id,
                            comment.date_created,
                            comment_id,
                        ],
                    )
                    return self.comment_in_to_out(comment_id, comment)
        except Exception as e:
            print(e)
            return {"message": "Could not update comment"}

    def is_authorized_to_update(self, comments_id: int, user_id: int) -> bool:
        """
        Check if the user is authorized to update the comment.
        References back to the get_one method to isolate a comment
        """
        comment = self.get_one(comments_id)
        if comment and comment.author_id == user_id:
            return True
        return False

    def get_all(self) -> Union[CommentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            content,
                            author_id,
                            rant_id,
                            date_created
                        FROM rant_comments
                        ORDER BY id;
                        """
                    )
                    result = []
                    for record in db:
                        print(record)  # This will print all comments
                        comment = CommentOut(
                            id=record[0],
                            content=record[1],
                            author_id=record[2],
                            rant_id=record[3],
                            date_created=record[4],
                        )
                        result.append(comment)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get all comments"}

    def get_all_comments_by_rant_id(
        self, rant_detail_id: int
    ) -> Union[CommentOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT
                            id,
                            content,
                            author_id,
                            rant_id,
                            date_created
                        FROM rant_comments
                        WHERE rant_id = %s;
                        """,
                        [
                            rant_detail_id
                        ],  # assigns rant_id to rant_detail_id param
                    )
                    rant_comments = db.fetchall()
                    if rant_comments:
                        rant_comments_list = []
                        for comments in rant_comments:
                            rant_comments_list.append(
                                CommentOut(
                                    id=comments[0],
                                    content=comments[1],
                                    author_id=comments[2],
                                    rant_id=comments[3],
                                    date_created=comments[4],
                                )
                            )
                        return rant_comments_list
                    else:
                        return []
        except Exception as e:
            print(f"ERROR! ERROR! ERROR!: {e}")
            return {"message": "Could not get all comments"}

    def create(self, comment: CommentIn) -> Union[CommentOut, Error]:
        # connect the database
        # bring in the pool variable from
        # queries.comments_pool import pool
        # get a cursor (something to run SQL with)
        # Run our INSERT statement
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                            INSERT INTO rant_comments
                                (
                                    content,
                                    author_id,
                                    rant_id,
                                    date_created
                                )
                            Values
                                (%s, %s, %s, %s)
                                RETURNING id;
                            """,
                        [
                            comment.content,
                            comment.author_id,
                            comment.rant_id,
                            comment.date_created,
                        ],
                    )
                    id = result.fetchone()[0]
                    # Return new data
                    return self.comment_in_to_out(id, comment)
        except Exception as e:
            print(e)
            error_message = str(e)
            return Error(message=error_message)

    def comment_in_to_out(self, id: int, comment: CommentIn):
        old_data = comment.dict()
        return CommentOut(id=id, **old_data)
