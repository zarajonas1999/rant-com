from pydantic import BaseModel
from typing import Optional
from queries.pool import pool


class Error(BaseModel):
    message: str


class CategoryOut(BaseModel):
    id: int
    name: str


class CategoryRepository:

    def get_one(self, category_id: int) -> Optional[CategoryOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT
                            id,
                            name
                        FROM categories
                        WHERE id = %s;
                        """,
                        [category_id],
                    )
                    record = result.fetchone()
                    # result = database, fetchone fetches one record
                    if record is None:
                        return None
                    category = CategoryOut(
                        id=record[0],
                        name=record[1],
                    )
                    return category
        except Exception as e:
            print(e)
            return Error(message="Could not get Category")
