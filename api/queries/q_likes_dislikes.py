from pydantic import BaseModel, ValidationError
from typing import Union, Optional
from queries.pool import pool
import psycopg.errors


class Error(BaseModel):
    message: Optional[str] = None
    user_id: Optional[int] = None
    rant_id: Optional[int] = None
    comment_id: Optional[int] = None


class LikesIn(BaseModel):
    user_id: int
    rant_id: int


class LikesOut(BaseModel):
    user_id: int
    rant_id: int


class CommentLikesIn(BaseModel):
    user_id: int
    comment_id: int


class CommentLikesOut(BaseModel):
    user_id: int
    comment_id: int


class RLikesRepo:
    def record_likes_out(self, record) -> LikesOut:
        return LikesOut(
            user_id=record[0],
            rant_id=record[1],
        )

    def get_rlikes(self, rant_id) -> int:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT COUNT(*) FROM rant_likes
                        WHERE rant_id = %s
                        """,
                        [rant_id]
                    )
                    result = db.fetchone()
                    if result:
                        return result[0]
                    else:
                        return 0
        except ValidationError as e:
            print(e.json())
            return Error(message="Error!" + str(e))

    def get_rdislikes(self, rant_id) -> int:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT COUNT(*) FROM rant_dislikes
                        WHERE rant_id = %s
                        """,
                        [rant_id]
                    )
                    result = db.fetchone()
                    if result:
                        return result[0]
                    else:
                        return 0
        except ValidationError as e:
            print(e.json())
            return Error(message="Error!" + str(e))

    def like(self, user_id, rant_id) -> Union[LikesOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM rant_dislikes
                        WHERE user_id = %s AND rant_id = %s
                        """,
                        [user_id, rant_id]
                    )
                    record = db.fetchone()

                    if record:
                        db.execute(
                            """
                            DELETE FROM rant_dislikes
                            WHERE user_id = %s AND rant_id = %s
                            """,
                            [record[0], record[1]]
                        )

                    db.execute(
                        """
                        INSERT INTO rant_likes
                            (
                                user_id
                                , rant_id
                            )
                        VALUES
                            (%s, %s)
                        """,
                        [
                            user_id,
                            rant_id,
                        ],
                    )
                    return LikesOut(
                        user_id=user_id,
                        rant_id=rant_id,
                    )
        except psycopg.errors.UniqueViolation:
            return Error(
                message="Already liked",
                user_id=user_id,
                rant_id=rant_id,
            )
        except Exception as e:
            return Error(message=f"Error: {e}")

    def dislike(self, user_id, rant_id) -> Union[LikesOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM rant_likes
                        WHERE user_id = %s AND rant_id = %s
                        """,
                        [user_id, rant_id]
                    )
                    record = db.fetchone()

                    if record:
                        db.execute(
                            """
                            DELETE FROM rant_likes
                            WHERE user_id = %s AND rant_id = %s
                            """,
                            [user_id, rant_id]
                        )

                    db.execute(
                        """
                        INSERT INTO rant_dislikes
                            (
                                user_id
                                , rant_id
                            )
                        VALUES
                            (%s, %s)
                        """,
                        [
                            user_id,
                            rant_id,
                        ],
                    )
                    return LikesOut(
                        user_id=user_id,
                        rant_id=rant_id
                    )

        except psycopg.errors.UniqueViolation:
            return Error(
                message="Already liked",
                user_id=user_id,
                rant_id=rant_id
            )
        except Exception as e:
            return Error(message=f"Error: {e}")


class CLikesRepo:
    def record_likes_out(self, record) -> CommentLikesOut:
        return CommentLikesOut(
            user_id=record[0],
            comment_id=record[1],
        )

    def get_clikes(self, comment_id) -> int:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT COUNT(*) FROM comment_likes
                        WHERE comment_id = %s
                        """,
                        [comment_id]
                    )
                    result = db.fetchone()
                    if result:
                        return result[0]
                    else:
                        return 0
        except Exception as e:
            return Error(message="Error!" + str(e))

    def get_cdislikes(self, comment_id) -> int:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT COUNT(*) FROM comment_dislikes
                        WHERE comment_id = %s
                        """,
                        [comment_id]
                    )
                    result = db.fetchone()
                    if result:
                        return result[0]
                    else:
                        return 0
        except Exception as e:
            return Error(message="Error!" + str(e))

    def like(self, user_id, comment_id) -> Union[CommentLikesOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM comment_dislikes
                        WHERE user_id = %s AND comment_id = %s
                        """,
                        [user_id, comment_id]
                    )
                    record = db.fetchone()

                    if record:
                        db.execute(
                            """
                            DELETE FROM comment_dislikes
                            WHERE user_id = %s AND comment_id = %s
                            """,
                            [record[0], record[1]]
                        )

                    db.execute(
                        """
                        INSERT INTO comment_likes
                            (
                                user_id
                                , comment_id
                            )
                        VALUES
                            (%s, %s)
                        """,
                        [
                            user_id,
                            comment_id,
                        ],
                    )
                    return CommentLikesOut(
                        user_id=user_id,
                        comment_id=comment_id,
                    )
        except psycopg.errors.UniqueViolation:
            return Error(
                message="Already liked",
                user_id=user_id,
                comment_id=comment_id,
            )
        except Exception as e:
            return Error(message=f"Error: {e}")

    def dislike(self, user_id, comment_id) -> Union[CommentLikesOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        SELECT * FROM comment_likes
                        WHERE user_id = %s AND comment_id = %s
                        """,
                        [user_id, comment_id]
                    )
                    record = db.fetchone()

                    if record:
                        db.execute(
                            """
                            DELETE FROM comment_likes
                            WHERE user_id = %s AND comment_id = %s
                            """,
                            [user_id, comment_id]
                        )

                    db.execute(
                        """
                        INSERT INTO comment_dislikes
                            (
                                user_id
                                , comment_id
                            )
                        VALUES
                            (%s, %s)
                        """,
                        [
                            user_id,
                            comment_id,
                        ],
                    )
                    return CommentLikesOut(
                        user_id=user_id,
                        comment_id=comment_id
                    )

        except psycopg.errors.UniqueViolation:
            return Error(
                message="Already liked",
                user_id=user_id,
                comment_id=comment_id
            )
        except Exception as e:
            return Error(message=f"Error: {e}")
