from pydantic import BaseModel
from queries.pool import pool
from typing import Union, List, Optional
from datetime import date


class Error(BaseModel):
    message: str


class RantsIn(BaseModel):
    title: str
    category_id: int
    content: str
    author_id: int  # this is a {user_id}
    date_created: date = date.today()


class RantsOut(BaseModel):
    id: str
    title: str
    category_id: int
    content: str
    author_id: int  # this is a {user_id}
    date_created: date


class RantLikeIn(BaseModel):
    user_id: int
    rant_id: int


class RantsRepository:
    def create(self, rants: RantsIn) -> RantsOut:
        with pool.connection() as conn:  # connect to database
            with conn.cursor() as db:  # get a cursor => Run SQL with
                result = db.execute(  # return data
                    """
                    INSERT INTO rants
                        (
                            title,
                            category_id,
                            content,
                            author_id,
                            date_created
                        )
                    VALUES
                        (%s, %s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        rants.title,
                        rants.category_id,
                        rants.content,
                        rants.author_id,
                        rants.date_created,
                    ],
                )
                id = result.fetchone()[0]
                # return RantsOut(id=id, **rants.dict())
                return self.rants_in_to_out(id, rants)

    def get_all(self) -> List[RantsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                        title,
                        category_id,
                        content,
                        author_id,
                        date_created
                        FROM rants
                        ORDER BY id;
                        """
                    )
                    result = []
                    for record in db.fetchall():
                        rants = RantsOut(
                            id=record[0],
                            title=record[1],
                            category_id=record[2],
                            content=record[3],
                            author_id=record[4],
                            date_created=record[5],
                        )
                        result.append(rants)
                    return result
        except Exception as e:
            return {"message": e}

    def update_rants(
        self, rants_id: int, rants: RantsIn
    ) -> Union[RantsOut, Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        UPDATE rants
                        SET title = %s,
                            category_id = %s,
                            content = %s,
                            author_id = %s,
                            date_created = %s
                        WHERE id = %s
                        """,
                        [
                            rants.title,
                            rants.category_id,
                            rants.content,
                            rants.author_id,
                            rants.date_created,
                            rants_id,
                        ],
                    )
                    return self.rants_in_to_out(rants_id, rants)
        except Exception as e:
            return {"Could not update rant": str(e)}

    def rants_in_to_out(self, id: int, rants: RantsIn):
        old_data = rants.dict()
        return RantsOut(id=id, **old_data)

    def delete_rants(self, rants_id: int):
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM rants
                        WHERE id = %s
                        """,
                        [rants_id],
                    )
                    return True
        except Exception as e:
            return {"error": "Could not delete rant", "details": str(e)}

    def get_one(self, rants_id: int) -> Optional[RantsOut]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id,
                        title,
                        category_id,
                        content,
                        author_id,
                        date_created
                        FROM rants
                        WHERE id = %s
                        """,
                        [rants_id],
                    )
                    record = result.fetchone()
                    return self.record_to_rants_out(record)
        except Exception as e:
            return {"Could not get rant": e}

    def record_to_rants_out(self, record):
        return RantsOut(
            id=record[0],
            title=record[1],
            category_id=record[2],
            content=record[3],
            author_id=record[4],
            date_created=record[5],
        )
