from fastapi import APIRouter, Depends
from typing import List, Union, Optional
from queries.q_rants import RantsIn, RantsRepository, RantsOut, Error

router = APIRouter()  # hook this up to main application


@router.post("/rants/new", response_model=RantsOut)
def create_rants(rants: RantsIn, repo: RantsRepository = Depends()):
    return repo.create(rants)


@router.get("/rants", response_model=List[RantsOut])
def get_all(repo: RantsRepository = Depends()):
    return repo.get_all()


@router.put("/rants/{rants_id}", response_model=Union[RantsOut, Error])
# def update_rants(
#     rants_id: int,
#     rants: RantsIn,
#     request: Request,
#     repo: RantsRepository = Depends(),
#     user: UserOut = Depends(authenticator.try_get_current_account_data),
# ) -> Union[Error, RantsOut]:
#     if user and authenticator.cookie_name in request.cookies:
#         if repo.is_authorized_to_update(rants_id, user.get("id")):
#             return repo.update_rants(rants_id, rants)
#         else:
#             raise HTTPException(
#                 status_code=403,
#                 detail="You are not authorized to edit this rant",
#             )
#     else:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Please log in or create an account to edit rant"
#         )
def update_rants(
    rants_id: int,
    rants: RantsIn,
    repo: RantsRepository = Depends(),
) -> Union[Error, RantsOut]:
    return repo.update_rants(rants_id, rants)


@router.delete("/rants/{rants_id}", response_model=bool)
def delete_rants(
    rants_id: int,
    repo: RantsRepository = Depends(),
) -> bool:
    return repo.delete_rants(rants_id)
    # else:
    #     raise HTTPException(
    #         status_code=403,
    #         detail="You are not authorized to delete this rant",
    #     )


@router.get("/rants/{rants_id}", response_model=Optional[RantsOut])
def get_one_rant(
    rants_id: int,
    repo: RantsRepository = Depends(),
) -> RantsOut:
    return repo.get_one(rants_id)
