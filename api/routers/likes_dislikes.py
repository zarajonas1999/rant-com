from fastapi import (
    APIRouter,
    Depends,
    Response,
    HTTPException,
    status,
    Request,
)
from queries.q_likes_dislikes import (
    LikesOut,
    CommentLikesOut,
    RLikesRepo,
    CLikesRepo,
    Error,
)
from queries.q_users import (
    UserOut
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator


class UserToken(Token):
    user: UserOut


router = APIRouter()


@router.get("/likes", response_model=int)
def get_rant_likes(
    rant_id: int,
    repo: RLikesRepo = Depends()
):
    return repo.get_rlikes(rant_id)


@router.get("/dislikes", response_model=int)
def get_rant_dislikes(
    rant_id: int,
    repo: RLikesRepo = Depends()
):
    return repo.get_rdislikes(rant_id)


@router.post("/likes", response_model=LikesOut)
def like_rant(
    user_id: int,
    rant_id: int,
    response: Response,
    request: Request,
    repo: RLikesRepo = Depends(),
    user: UserOut = Depends(authenticator.try_get_current_account_data),
):
    if user and authenticator.cookie_name in request.cookies:
        try:
            return repo.like(user_id, rant_id)
        except Exception as e:
            response.status_code = 418
            return Error(message=e)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please log in",
        )


@router.post("/dislikes", response_model=LikesOut)
def dislike_rant(
    user_id: int,
    rant_id: int,
    response: Response,
    request: Request,
    repo: RLikesRepo = Depends(),
    user: UserOut = Depends(authenticator.try_get_current_account_data),
):
    if user and authenticator.cookie_name in request.cookies:
        try:
            return repo.dislike(user_id, rant_id)
        except Exception as e:
            response.status_code = 418
            return Error(message=e)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please log in",
        )


@router.get("/comment_likes", response_model=int)
def get_comment_likes(
    comment_id: int,
    repo: CLikesRepo = Depends()
):
    return repo.get_clikes(comment_id)


@router.get("/comment_dislikes", response_model=int)
def get_comment_dislikes(
    comment_id: int,
    repo: CLikesRepo = Depends()
):
    return repo.get_cdislikes(comment_id)


@router.post("/comment_likes", response_model=CommentLikesOut)
def like_comment(
    user_id: int,
    comment_id: int,
    response: Response,
    request: Request,
    repo: CLikesRepo = Depends(),
    user: UserOut = Depends(authenticator.try_get_current_account_data),
):
    if user and authenticator.cookie_name in request.cookies:
        try:
            return repo.like(user_id, comment_id)
        except Exception as e:
            response.status_code = 418
            return Error(message=e)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please log in",
        )


@router.post("/comment_dislikes", response_model=CommentLikesOut)
def dislike_comment(
    user_id: int,
    comment_id: int,
    response: Response,
    request: Request,
    repo: CLikesRepo = Depends(),
    user: UserOut = Depends(authenticator.try_get_current_account_data),
):
    if user and authenticator.cookie_name in request.cookies:
        try:
            return repo.dislike(user_id, comment_id)
        except Exception as e:
            response.status_code = 418
            return Error(message=e)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please log in",
        )
