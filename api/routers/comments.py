from fastapi import (
    APIRouter,
    Depends,
    Response,
    HTTPException,
    status,
    Request,
)
from typing import Union, List, Optional
from queries.q_comments import CommentIn, CommentRepository, CommentOut, Error
from queries.q_users import UserOut
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator

router = APIRouter()


class UserToken(Token):
    user: UserOut


@router.post("/comments", response_model=Union[CommentOut, Error])
def create_comment(
    comment: CommentIn,
    response: Response,
    request: Request,
    repo: CommentRepository = Depends(),
    user: UserOut = Depends(authenticator.try_get_current_account_data),
):
    if user and authenticator.cookie_name in request.cookies:
        try:
            return repo.create(comment)
        except Exception as e:
            response.status_code = 400
            error_message = str(e)
            return Error(message=error_message)
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please log in or create an account to post a comment",
        )


@router.get("/comments", response_model=Union[List[CommentOut], Error])
def get_all(
    repo: CommentRepository = Depends(),
):
    return repo.get_all()


@router.put("/comments/{comments_id}", response_model=Union[CommentOut, Error])
def update_comment(
    comments_id: int,
    comment: CommentIn,
    request: Request,
    repo: CommentRepository = Depends(),
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> Union[CommentOut, Error]:
    if user and authenticator.cookie_name in request.cookies:
        if repo.is_authorized_to_update(comments_id, user.get("id")):
            return repo.update(comments_id, comment)
        else:
            raise HTTPException(
                status_code=403,
                detail="You are not authorized to edit this comment",
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please log in or create an account to edit a comment",
        )


@router.delete("/comments/{comments_id}", response_model=bool)
def delete_comment(
    comments_id: int,
    request: Request,
    repo: CommentRepository = Depends(),
    user: UserOut = Depends(authenticator.try_get_current_account_data),
) -> bool:
    if user and authenticator.cookie_name in request.cookies:
        if repo.is_authorized_to_update(comments_id, user.get("id")):
            return repo.delete(comments_id)
        else:
            raise HTTPException(
                status_code=403,
                detail="You are not authorized to delete this comment",
            )
    else:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Please log in or create an account to delete a comment",
        )


@router.get("/comments/{comments_id}", response_model=Optional[CommentOut])
def get_one_comment(
    comments_id: int,  # No comments_id field in rant_ comments table, just id
    response: Response,
    repo: CommentRepository = Depends(),
) -> CommentOut:
    comment = repo.get_one(comments_id)
    if comment is None:
        response.status_code = 404
    return comment


@router.get(
    "/comments/rant/{rant_detail_id}",
    response_model=Union[List[CommentOut], Error],
)
def get_all_specific_rant_comments(
    rant_detail_id: int,
    repo: CommentRepository = Depends(),
):
    return repo.get_all_comments_by_rant_id(rant_detail_id)
