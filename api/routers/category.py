from fastapi import (
    APIRouter,
    Depends,
    Response,
)
from queries.q_category import CategoryOut, CategoryRepository
from typing import Optional

router = APIRouter()


@router.get("/category/{category_id}", response_model=Optional[CategoryOut])
def get_one_category(
    category_id: int,
    response: Response,
    repo: CategoryRepository = Depends(),
) -> CategoryOut:
    category = repo.get_one(category_id)
    if category is None:
        response.status_code = 404
    return category
