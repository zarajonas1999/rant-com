from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from jwtdown_fastapi.authentication import Token
from authenticator import authenticator
from typing import List, Optional, Union
from pydantic import BaseModel
from queries.q_users import (
    UserIn,
    UserOut,
    UserRantsOut,
    UserCommentsOut,
    UserRepo,
    Error,
    DuplicateUserError,
)


class UserForm(BaseModel):
    username: str
    password: str


class UserToken(Token):
    user: UserOut


class HttpError(BaseModel):
    detail: str


router = APIRouter()


@router.get("/protected", response_model=bool)
async def check_token(
    request: Request,
    user_data: dict = Depends(authenticator.get_current_account_data)
):
    return True


@router.get("/token", response_model=UserToken | None)
async def get_token(
    request: Request,
    user: UserOut = Depends(authenticator.try_get_current_account_data)
):
    if user and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "user": user,
        }


@router.post("/user", response_model=UserToken | HttpError)
async def create_user(
    info: UserIn,
    request: Request,
    response: Response,
    repo: UserRepo = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        user = repo.create(info, hashed_password)
    except DuplicateUserError:
        print("aaaaa")
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Cannot create user with those credentials",
        )
    form = UserForm(username=info.username, password=info.password)
    token = await authenticator.login(response, request, form, repo)
    print("token", token)
    return UserToken(user=user, **token.dict())


@router.get("/user", response_model=List[UserOut])
def get_all(repo: UserRepo = Depends()):
    return repo.get_all()


@router.get("/user/{user_id}", response_model=Optional[UserOut])
def get_detail(
    user_id: int,
    request: Request,
    response: Response,
    repo: UserRepo = Depends(),
) -> Optional[UserOut]:
    result = repo.get_detail(user_id)
    if result:
        if isinstance(result, Error):
            response.status_code = 418
        return result


@router.get("/user/{username}/rants", response_model=List[UserRantsOut])
def get_user_rants(
    username: str,
    repo: UserRepo = Depends(),
) -> List[UserRantsOut]:
    result = repo.get_user_rants(username)
    return result


@router.get("/user/{username}/comments", response_model=List[UserCommentsOut])
def get_user_comments(
    username: str,
    repo: UserRepo = Depends(),
) -> List[UserRantsOut]:
    result = repo.get_user_comments(username)
    return result


@router.put("/user/{user_id}", response_model=Union[UserOut, Error])
def update_user(
    user_id: int,
    user: UserIn,
    logged_user: UserOut = Depends(authenticator.try_get_current_account_data),
    repo: UserRepo = Depends(),
) -> Union[Error, UserOut]:
    # check if account to be updated == logged user's account
    print("logged user: ", logged_user.get("id"))
    if not logged_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not currently logged in"
        )
    elif logged_user and logged_user.get("id") != user_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not yours!"
        )
    hashed_password = authenticator.hash_password(user.password)
    user.password = hashed_password
    return repo.update(user_id, user)


@router.delete("/user/{user_id}", response_model=bool)
def delete_user(
    user_id: int,
    response: Response,
    request: Request,
    logged_user: UserOut = Depends(authenticator.try_get_current_account_data),
    repo: UserRepo = Depends(),
) -> bool:
    # check if account to be deleted == logged user's account
    print("logged user: ", logged_user)
    if not logged_user:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not currently logged in"
        )
    elif logged_user and logged_user.get("id") != user_id:
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Not yours!"
        )
    authenticator.logout(request, response)
    repo.delete(user_id)
    return True
