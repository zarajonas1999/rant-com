from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import rants
import os
from routers import users, comments, likes_dislikes, category
from authenticator import authenticator


app = FastAPI()
app.include_router(rants.router)
app.include_router(users.router)
app.include_router(authenticator.router)
app.include_router(comments.router)
app.include_router(likes_dislikes.router)
app.include_router(category.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST"),
        "http://localhost:5173"
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/launch-details")
def launch_details():
    return {
        "launch_details": {
            "module": 3,
            "week": 17,
            "day": 5,
            "hour": 19,
            "min": "00"
        }
    }


@app.get("/")
def root():
    return {"message": "You hit the root path!"}

# Test comment
