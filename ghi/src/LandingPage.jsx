import { Link } from 'react-router-dom'
import Hero from './Components/Hero'
import { Button } from 'flowbite-react'
import './App.css'

function LandingPage() {
    return (
        <div>
            <div className="overlay">
                <Hero />
            </div>
            <div className="flex flex-col items-center justify-center min-h-screen">
                <div className="absolute inset-0 flex items-center justify-center bg-transparent">
                    <h1
                        className="text-4xl justify-center font-semibold mt-4 text-white"
                        style={{
                            fontFamily: 'frijole',
                            fontSize: '6rem',
                            paddingBottom: '35px',
                        }}
                    >
                        Welcome to Rant
                    </h1>
                    <div className="hero-btns">
                        <Button
                            color="gray"
                            buttonStyle="btn--outline"
                            buttonSize="btn--large"
                            style={{
                                fontSize: '44px',
                                padding: '15px 30px',
                                fontFamily: 'frijole',
                            }}
                        >
                            <Link
                                to={`/rants`}
                                style={{
                                    fontFamily: 'frijole',
                                    fontSize: '40px',
                                }}
                            >
                                View Rants
                            </Link>
                        </Button>
                        <Button
                            color="gray"
                            buttonStyle="btn--outline"
                            buttonSize="btn--large"
                            style={{
                                fontSize: '44px',
                                padding: '15px 30px',
                                fontFamily: 'frijole',
                            }}
                        >
                            <Link
                                to={`/user`}
                                style={{
                                    fontFamily: 'frijole',
                                    fontSize: '40px',
                                }}
                            >
                                Sign In / Up
                            </Link>
                        </Button>{' '}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LandingPage

