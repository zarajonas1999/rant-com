import { useState, useEffect } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import { useCallback } from 'react';
// useParams => Returns an object of key, value pairs of dyanmic params from current URL
import { useAuthContext } from '@galvanize-inc/jwtdown-for-react';

function RantUpdate() {
    const { token } = useAuthContext()
    const [title, setTitle] = useState('')
    const [categoryId, setCategoryId] = useState('')
    const [authorId, setAuthorId] = useState('')
    const [content, setContent] = useState('')
    const params = useParams()
    const [rantId, setRantId] = useState(null)
    const navigate = useNavigate()
    const baseUrl = import.meta.env.VITE_API_HOST

    useEffect(() => {
        if (params.rants_id !== rantId) {
            setRantId(params.rants_id)
        }
    }, [params.rants_id, rantId])

    const getRantDetails = useCallback(async () => {
        try {
            const rantUrl = `${baseUrl}/rants/${rantId}`
            const response = await fetch(rantUrl, {
                headers: { Authorization: `Bearer ${token}` },
            })
            if (response.ok) {
                const data = await response.json()
                console.log(params)
                setTitle(data.title)
                setCategoryId(data.category_id)
                setContent(data.content)
                setAuthorId(data.author_id)
                console.log('LKDSJF:LKSDJ', setRantId)
            }
        } catch (error) {
            console.error('Failed to fetch rant details:', error)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token, rantId])

    useEffect(() => {
        if (rantId) {
            getRantDetails()
            console.log('LOGSGSGSG', params)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [rantId, getRantDetails])

    const handleTitleChange = (e) => {
        setTitle(e.target.value)
    }

    const handleCategoryChange = (e) => {
        setCategoryId(e.target.value)
    }

    const handleContentChange = (e) => {
        setContent(e.target.value)
    }

    const handleSubmit = async (e) => {
        e.preventDefault()

        const updatedRant = {
            id: rantId,
            title: title,
            category_id: categoryId,
            content: content,
            author_id: authorId,
        }

        try {
            const response = await fetch(`${baseUrl}/rants/${rantId}`, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
                body: JSON.stringify(updatedRant),
            })
            if (response.ok) {
                navigate(`/rants/${rantId}`)
            }
        } catch (error) {
            console.error('Failed to update rant:', error)
        }
    }

    // const getAuthors = async () => {
    //     const authorsURL = `${baseUrl}/user/`
    //     const authorsResponse = await fetch(authorsURL)
    //     if (authorsResponse.ok) {
    //         const authorsData = await authorsResponse.json()
    //         // console.log(authorsData)
    //         setRantUsersData(authorsData)
    //     }
    // }
    // const matchingRantUser =
    //     rantUsers !== null
    //         ? rantUsers.find((rantUser) => rantUser.id === rant?.author_id)
    //         : null
    // author will be matchingRantUser?.username, use this in jsxhtml

    const deleteRant = async () => {
        try {
            const response = await fetch(`${baseUrl}/rants/${rantId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${token}`,
                },
            })
            if (response.ok) {
                navigate('/rants')
            }
        } catch (error) {
            console.error('Failed to delete rant:', error)
        }
    }

    const categories = [
        { id: 1, name: 'Pet' },
        { id: 2, name: 'Software' },
        { id: 3, name: 'Hardware' },
        { id: 4, name: 'Movies' },
        { id: 5, name: 'TV' },
        { id: 6, name: 'Music' },
        { id: 7, name: 'Products' },
        { id: 8, name: 'News' },
        { id: 9, name: 'Shenanigans' },
    ]

    return (
        <div>
            <h1>Ive Changed My Mind</h1>
            <form onSubmit={handleSubmit} id="update-rant-form">
                <div className="form-floating mb-3">
                    <input
                        value={title}
                        onChange={handleTitleChange}
                        placeholder="Title"
                        required
                        type="text"
                        name="title"
                        id="title"
                        className="form-control"
                    />
                    <label htmlFor="title">Update Title</label>
                </div>
                <div className="form-floating mb-3">
                    <select
                        value={categoryId}
                        onChange={handleCategoryChange}
                        name="category"
                        id="category"
                        className="form-select d-inline-flex"
                    >
                        <option value="">What is this about?!</option>
                        {categories.map((category) => (
                            <option key={category.id} value={category.id}>
                                {category.name}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="form-floating mb-3">
                    <textarea
                        value={content}
                        onChange={handleContentChange}
                        placeholder="Content"
                        name="content"
                        id="content"
                        required
                        style={{ height: '300px' }}
                        className="form-control"
                    />
                    <label htmlFor="content">Carry On With The Rant</label>
                </div>
                <button type="submit" className="btn btn-primary me-2">
                    Update
                </button>
                <button
                    type="button"
                    className="btn btn-danger"
                    onClick={deleteRant}
                >
                    Delete
                </button>
            </form>
        </div>
    )
}

export default RantUpdate
