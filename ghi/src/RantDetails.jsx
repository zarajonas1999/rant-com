import { useState, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { AiFillLike, AiFillDislike } from 'react-icons/ai'
import './App.css'
import { Card, Button, Label, TextInput, Blockquote } from 'flowbite-react'

function RantDetails() {
    const { rants_id } = useParams()
    const { fetchWithCookie } = useToken()
    const baseUrl = import.meta.env.VITE_API_HOST

    const [rant, setRant] = useState(null)
    const [rantComments, setRantComments] = useState(null)
    const fetchData = async () => {
        if (rants_id) {
            const rantURL = `${baseUrl}/rants/${rants_id}`
            const rantResponse = await fetch(rantURL)
            const rantCommentsURL = `${baseUrl}/comments/rant/${rants_id}`
            const rantCommentsResponse = await fetch(rantCommentsURL)

            // handle Likes & Dislikes
            if (rantResponse.ok) {
                const rantData = await rantResponse.json()
                const likeResponse = await fetch(
                    `${baseUrl}/likes/?rant_id=${rants_id}`
                )
                const dislikeResponse = await fetch(
                    `${baseUrl}/dislikes/?rant_id=${rants_id}`
                )
                if (likeResponse.ok && dislikeResponse.ok) {
                    const likeData = await likeResponse.json()
                    const dislikeData = await dislikeResponse.json()
                    const spice = likeData - dislikeData
                    rantData.likes = spice
                } else {
                    console.error(
                        'Unable to count likes ',
                        likeResponse.statusText
                    )
                }

                setRant(rantData)
            }
            if (rantCommentsResponse.ok) {
                const rantCommentsData = await rantCommentsResponse.json()

                const commentsWithLikes = await Promise.all(
                    rantCommentsData.map(async (comment) => {
                        const likeResponse = await fetch(
                            `${baseUrl}/comment_likes/?comment_id=${comment.id}`
                        )
                        const dislikeResponse = await fetch(
                            `${baseUrl}/comment_dislikes/?comment_id=${comment.id}`
                        )
                        if (likeResponse.ok && dislikeResponse.ok) {
                            const likeData = await likeResponse.json()
                            const dislikeData = await dislikeResponse.json()
                            const spice = likeData - dislikeData
                            return {
                                ...comment,
                                likes: spice,
                            }
                        } else {
                            console.error(
                                'Unable to count likes ',
                                likeResponse.statusText
                            )
                            return { ...comment, likes: 0 }
                        }
                    })
                )
                // console.log(rantCommentsData)
                setRantComments(
                    commentsWithLikes.sort(
                        (a, b) =>
                            new Date(b.date_created) - new Date(a.date_created)
                    )
                )
            }
        }
    }

    const categories = [
        { id: 1, name: 'Pet' },
        { id: 2, name: 'Software' },
        { id: 3, name: 'Hardware' },
        { id: 4, name: 'Movies' },
        { id: 5, name: 'TV' },
        { id: 6, name: 'Music' },
        { id: 7, name: 'Products' },
        { id: 8, name: 'News' },
        { id: 9, name: 'Shenanigans' },
    ]
    const matchingCategory = categories.find(
        (category) => category.id === rant?.category_id
    )

    const [rantUsers, setRantUsersData] = useState(null)
    const getAuthors = async () => {
        const authorsURL = `${baseUrl}/user/`
        const authorsResponse = await fetch(authorsURL)
        if (authorsResponse.ok) {
            const authorsData = await authorsResponse.json()
            // console.log(authorsData)
            setRantUsersData(authorsData)
        }
    }
    const matchingRantUser =
        rantUsers !== null
            ? rantUsers.find((rantUser) => rantUser.id === rant?.author_id)
            : null
    // author will be matchingRantUser?.username, use this in jsxhtml

    const [userId, setUserId] = useState(null)
    const getLoggedUser = async () => {
        const userData = await fetchWithCookie(`${baseUrl}/token/`)
        if (!userData) {
            console.log('Please log in')
            return
        }

        setUserId(userData.user.id)
    }

    const [newComment, setNewComment] = useState('')
    const postComment = async (event) => {
        event.preventDefault()
        if (newComment && userId) {
            const commentData = {
                content: newComment,
                author_id: userId,
                rant_id: rants_id,
            }
            const response = await fetch(`${baseUrl}/comments`, {
                method: 'POST',
                credentials: 'include',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(commentData),
            })
            if (response.ok) {
                const newCommentData = await response.json()
                setRantComments([...rantComments, newCommentData])
                setNewComment('')
            }
        }
    }

    const handleRantLike = async (rant) => {
        try {
            if (userId) {
                const response = await fetch(
                    `${baseUrl}/likes?user_id=${userId}&rant_id=${rant.id}`,
                    {
                        method: 'post',
                        headers: {
                            'content-type': 'application/json',
                        },
                        credentials: 'include',
                    }
                )

                if (response.ok) {
                    fetchData()
                } else {
                    console.error('unable to like ', response.statustext)
                }
            }
        } catch (error) {
            console.error('error: ', error)
        }
    }

    const handleRantDislike = async (rant) => {
        try {
            if (userId) {
                const response = await fetch(
                    `${baseUrl}/dislikes?user_id=${userId}&rant_id=${rant.id}`,
                    {
                        method: 'post',
                        headers: {
                            'content-type': 'application/json',
                        },
                        credentials: 'include',
                    }
                )

                if (response.ok) {
                    fetchData()
                } else {
                    console.error('unable to dislike ', response.statustext)
                }
            }
        } catch (error) {
            console.error('error: ', error)
        }
    }

    const handleCommentLike = async (comment) => {
        try {
            if (userId) {
                const response = await fetch(
                    `${baseUrl}/comment_likes?user_id=${userId}&comment_id=${comment.id}`,
                    {
                        method: 'post',
                        headers: {
                            'content-type': 'application/json',
                        },
                        credentials: 'include',
                    }
                )

                if (response.ok) {
                    fetchData()
                } else {
                    console.error('unable to like ', response.statusText)
                }
            }
        } catch (error) {
            console.error('error: ', error)
        }
    }

    const handleCommentDislike = async (comment) => {
        try {
            if (userId) {
                const response = await fetch(
                    `${baseUrl}/comment_dislikes?user_id=${userId}&comment_id=${comment.id}`,
                    {
                        method: 'post',
                        headers: {
                            'content-type': 'application/json',
                        },
                        credentials: 'include',
                    }
                )

                if (response.ok) {
                    fetchData()
                } else {
                    console.error('unable to dislike ', response.statusText)
                }
            }
        } catch (error) {
            console.error('error: ', error)
        }
    }

    const [editCommentId, setEditCommentId] = useState(null)
    const [editingComment, setEditingComment] = useState(null)
    const handleEditingCommentChange = (event, commentId) => {
        setEditingComment(event.target.value)
        setEditCommentId(commentId)
    }
    const editComment = async (editCommentId) => {
        if (editingComment && userId) {
            const editedComment = rantComments.find(
                (c) => c.id === editCommentId
            )
            const updatedComment = {
                content: editingComment,
                author_id: userId,
                likes: editedComment.likes,
                dislikes: editedComment.dislikes,
                rant_id: rants_id,
                date_created: editedComment.date_created,
            }
            const response = await fetch(
                `${baseUrl}/comments/${editCommentId}`,
                {
                    method: 'PUT',
                    credentials: 'include',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(updatedComment),
                }
            )
            if (response.ok) {
                const updatedCommentData = await response.json()
                const updatedComments = rantComments.map((comment) =>
                    comment.id === editCommentId ? updatedCommentData : comment
                )
                setRantComments(updatedComments)
                setEditingComment(null)
                setEditCommentId(null)
            }
        }
    }

    const deleteComment = async (editCommentId) => {
        if (userId) {
            const response = await fetch(
                `${baseUrl}/comments/${editCommentId}`,
                {
                    method: 'DELETE',
                    credentials: 'include',
                }
            )
            if (response.ok) {
                const updatedComments = rantComments.filter(
                    (comment) => comment.id !== editCommentId
                )
                setRantComments(updatedComments)
                setEditingComment(null)
                setEditCommentId(null)
            }
        }
    }

    useEffect(() => {
        getLoggedUser()
        getAuthors()
        fetchData()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [rants_id])

    return (
        // <div className="absolute inset-0 flex flex-col items-center justify-center">
        <div className="inset-0 flex flex-col items-center justify-center">
            <Blockquote className="text-center w-full">
                Embrace the power of self-expression and share your thoughts
                with confidence, as the beauty of opinions lies in the diversity
                they bring to the world, enriching our perspectives, even when
                not entirely positive, by fostering growth and understanding.
            </Blockquote>
            <div>
                <br></br>
                <div className="inset-0 flex flex-col items-center justify-center">
                    <h1>{matchingCategory?.name}</h1>
                    <br></br>
                    <Card className="max-w-sm">
                        <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                            {rant?.title}
                        </h5>
                        <p>{rant?.content}</p>
                        <p className="font-normal text-gray-900 dark:text-white">
                            Author:{' '}
                            <Link to={`/user/${matchingRantUser?.username}`}>
                                {matchingRantUser?.username}
                            </Link>
                        </p>
                        <p className="font-normal text-gray-900 dark:text-white flex items-center">
                            Spice:
                            <div className="flex items-center">
                                <AiFillLike
                                    color="red"
                                    onClick={() => handleRantLike(rant)}
                                    style={{ cursor: 'pointer' }}
                                />
                                {rant?.likes}
                                <AiFillDislike
                                    color="blue"
                                    onClick={() => handleRantDislike(rant)}
                                    style={{ cursor: 'pointer' }}
                                />
                            </div>
                        </p>
                        <p className="font-normal text-gray-900 dark:text-white">
                            Posted: {rant?.date_created}
                        </p>
                    </Card>
                </div>
                <br></br>
                <br></br>
                <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    Comments
                </h5>
                <br></br>
                <div>
                    <div className="mb-2 block">
                        <Label
                            htmlFor="base"
                            value="Now, how does this rant make you feel? :)"
                        />
                    </div>
                    <TextInput
                        id="base"
                        type="text"
                        sizing="md"
                        value={newComment}
                        onChange={(e) => setNewComment(e.target.value)}
                    />
                    <div className="flex flex-wrap gap-2">
                        <Button onClick={postComment} color="dark">
                            Post Comment
                        </Button>
                    </div>
                </div>
                <br></br>
                <br></br>
                <div>
                    {rantComments &&
                        rantComments.map((comment) => {
                            const author = rantUsers?.find(
                                (author) => author.id === comment.author_id
                            )
                            const isAuthorizedToUpdateOrDelete =
                                userId === comment.author_id
                            return (
                                <div key={comment.id}>
                                    <Card key={comment.id} className="max-w-sm">
                                        <p>{comment.content}</p>
                                        {isAuthorizedToUpdateOrDelete ? (
                                            <>
                                                {editCommentId ===
                                                comment.id ? (
                                                    <input
                                                        type="text"
                                                        value={editingComment}
                                                        onChange={(e) =>
                                                            handleEditingCommentChange(
                                                                e,
                                                                comment.id
                                                            )
                                                        }
                                                    />
                                                ) : null}
                                                {editCommentId ===
                                                comment.id ? (
                                                    <Button
                                                        onClick={() =>
                                                            editComment(
                                                                comment.id
                                                            )
                                                        }
                                                        color="dark"
                                                    >
                                                        Alter
                                                    </Button>
                                                ) : null}
                                                {editCommentId ===
                                                comment.id ? (
                                                    <Button
                                                        onClick={() =>
                                                            deleteComment(
                                                                comment.id
                                                            )
                                                        }
                                                        color="dark"
                                                    >
                                                        Obliderate
                                                    </Button>
                                                ) : null}
                                                {editCommentId ===
                                                comment.id ? (
                                                    <Button
                                                        onClick={() =>
                                                            setEditCommentId(
                                                                null
                                                            )
                                                        }
                                                        color="dark"
                                                    >
                                                        (~^O^~) NVM (^p^)y
                                                    </Button>
                                                ) : null}
                                            </>
                                        ) : null}
                                        <p className="font-normal text-gray-900 dark:text-white">
                                            Spice:
                                            <div className="flex items-center">
                                                <AiFillLike
                                                    color="red"
                                                    onClick={() =>
                                                        handleCommentLike(
                                                            comment
                                                        )
                                                    }
                                                    style={{
                                                        cursor: 'pointer',
                                                    }}
                                                />
                                                {comment?.likes}
                                                <AiFillDislike
                                                    color="blue"
                                                    onClick={() =>
                                                        handleCommentDislike(
                                                            comment
                                                        )
                                                    }
                                                    style={{
                                                        cursor: 'pointer',
                                                    }}
                                                />
                                            </div>
                                        </p>
                                        <p className="font-normal text-gray-900 dark:text-white">
                                            Posted: {comment.date_created}
                                        </p>
                                        <p className="font-normal text-gray-900 dark:text-white">
                                            Author:{' '}
                                            <Link
                                                to={`/user/${author?.username}`}
                                            >
                                                {author?.username}
                                            </Link>
                                        </p>
                                        {editCommentId === null ? (
                                            <Button
                                                size="sm"
                                                onClick={() =>
                                                    setEditCommentId(comment.id)
                                                }
                                                color="dark"
                                                className="self-start"
                                            >
                                                Edit
                                            </Button>
                                        ) : null}
                                    </Card>
                                    <br></br>
                                    <br></br>
                                </div>
                            )
                        })}
                </div>
            </div>
        </div>
    )
}

export default RantDetails
