import rantsVid from '../Assets/rants.mp4'
import './Component.css'

function Hero() {
    return (
        <div className="w-full h-screen relative">
            <video
                className="w-full h-full object-cover"
                src={rantsVid}
                autoPlay
                loop
                muted
            />
        </div>
    )
}

export default Hero
