import './Component.css'

function Footer() {
    return (
        <footer
            className="w-screen h-50px footer bg-black p-8"
        >
            <div className="items-center justify-center gap-y-6 gap-x-12 bg-red text-center md:justify-center">
                <ul className="flex flex-wrap text-center justify-center gap-y-2 gap-x-8">
                    <li>
                        <a
                            href="https://www.linkedin.com/in/chase-fienhold-43a395299/"
                            className="text-white font-normal transition-colors hover:text-red-800"
                            style={{
                                fontSize: '15px',
                                padding: '15px 30px',
                                fontFamily: 'frijole',
                            }}
                        >
                            Chase Fienhold
                        </a>
                    </li>
                    <li>
                        <a
                            href="https://www.linkedin.com/in/jonas-cbz/"
                            className="text-white font-normal transition-colors hover:text-red-800"
                            style={{
                                fontSize: '15px',
                                padding: '15px 30px',
                                fontFamily: 'frijole',
                            }}
                        >
                            Jonas Zara
                        </a>
                    </li>
                    <li>
                        <a
                            href="https://www.linkedin.com/in/johanson-bombaes/"
                            className="text-white font-normal transition-colors hover:text-red-800"
                            style={{
                                fontSize: '15px',
                                padding: '15px 30px',
                                fontFamily: 'frijole',
                            }}
                        >
                            Johanson Bombaes
                        </a>
                    </li>
                    <li>
                        <a
                            href="https://www.linkedin.com/in/mendozaedgar02/"
                            className="text-white font-normal transition-colors hover:text-red-800"
                            style={{
                                fontSize: '15px',
                                padding: '15px 30px',
                                fontFamily: 'frijole',
                            }}
                        >
                            Edgar Mendoza
                        </a>
                    </li>
                </ul>
            </div>
        </footer>
    )
}

export default Footer
