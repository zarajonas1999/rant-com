import { useState, useEffect } from 'react'
import { AiOutlineClose, AiOutlineMenu } from 'react-icons/ai'
import { NavLink } from 'react-router-dom'
import { Dropdown, DropdownItem } from 'flowbite-react'
import useToken from '@galvanize-inc/jwtdown-for-react'
import './Component.css'

const Navibar = () => {
    const [nav, setNav] = useState(false)
    const [loggedInUsername, setLoggedInUsername] = useState(null)
    const { token, fetchWithCookie } = useToken()
    const baseUrl = import.meta.env.VITE_API_HOST

    const getUsername = async () => {
        const userData = await fetchWithCookie(`${baseUrl}/token/`)
        if (!userData) {
            console.log('Not currently logged in')
            setLoggedInUsername(null)
            return
        }

        setLoggedInUsername(userData.user.username)
    }

    const handleNav = () => {
        setNav(!nav)
    }

    const categories = [
        { id: 1, name: 'Pet', link: '/category/1' },
        { id: 2, name: 'Software', link: '/category/2' },
        { id: 3, name: 'Hardware', link: '/category/3' },
        { id: 4, name: 'Movies', link: '/category/4' },
        { id: 5, name: 'TV', link: '/category/5' },
        { id: 6, name: 'Music', link: '/category/6' },
        { id: 7, name: 'Products', link: '/category/7' },
        { id: 8, name: 'News', link: '/category/8' },
        { id: 9, name: 'Shenanigans', link: '/category/9' },
    ]

    const rantsOptions = [
        { id: 1, name: 'Rant List', link: '/rants' },
        { id: 2, name: 'Create Rant', link: '/rants/new' },
        { id: 3, name: 'Recent Rants', link: '/rants' },
    ]

    useEffect(() => {
        getUsername()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])

    return (
        <div className="bg-black text-red-500 w-screen fixed top-0 left-0 z-50">
            <div className="max-w-[1200px] mx-auto h-[100px] flex justify-between items-center px-4">
                <h1
                    className="text-3xl font-bold"
                    style={{
                        fontSize: '30px',
                        padding: '15px 30px',
                        fontFamily: 'frijole',
                        backgroundImage:
                            'linear-gradient(to right, #FFFFFF, #6a1d07, #90280a)',
                        WebkitBackgroundClip: 'text',
                        color: 'transparent',
                    }}
                >
                    <NavLink
                        to="/"
                        className="text-blue-gray hover:text-red-900 transition-colors"
                    >
                        RANTS.COM
                    </NavLink>{' '}
                </h1>
                <ul className="hidden md:flex space-x-6">
                    <li
                        className="p-2 font-medium"
                        style={{
                            fontSize: '15px',
                            padding: '15px 30px',
                            fontFamily: 'frijole',
                            backgroundImage:
                                'linear-gradient(to right, #FFFFFF, #6a1d07, #90280a)',
                            WebkitBackgroundClip: 'text',
                            color: 'transparent',
                            pointerEvents: 'none',
                        }}
                    >
                        <span className="text-orange hover:text-red-900 transition-colors">
                            Rant
                        </span>
                    </li>
                    <li className="p-2 font-medium relative">
                        <Dropdown
                            label={null}
                            position="bottom-start"
                            dismissOnClick={true}
                        >
                            {rantsOptions.map((rant) => (
                                <DropdownItem key={rant.id}>
                                    <NavLink
                                        to={rant.link}
                                        className="text-red"
                                    >
                                        {rant.name}
                                    </NavLink>
                                </DropdownItem>
                            ))}
                        </Dropdown>
                    </li>

                    <li
                        className="p-2 font-medium"
                        style={{
                            fontSize: '15px',
                            padding: '15px 30px',
                            fontFamily: 'frijole',
                            backgroundImage:
                                'linear-gradient(to right, #FFFFFF, #6a1d07, #90280a)',
                            WebkitBackgroundClip: 'text',
                            color: 'transparent',
                            pointerEvents: 'none',
                        }}
                    >
                        <span className="text-orange hover:text-red-900 transition-colors">
                            Categories
                        </span>
                    </li>
                    <li className="p-2 font-medium relative">
                        <Dropdown
                            label={null}
                            position="bottom-start"
                            // arrow={false}
                            dismissOnClick={true}
                        >
                            {categories.map((category) => (
                                <DropdownItem key={category.id}>
                                    <NavLink
                                        to={category.link}
                                        className="text-red"
                                    >
                                        {category.name}
                                    </NavLink>
                                </DropdownItem>
                            ))}
                        </Dropdown>
                    </li>
                    <li
                        className="p-2 font-medium"
                        style={{
                            fontSize: '15px',
                            padding: '15px 30px',
                            fontFamily: 'frijole',
                            backgroundImage:
                                'linear-gradient(to right, #441205, #6a1d07, #90280a)',
                            WebkitBackgroundClip: 'text',
                            color: 'transparent',
                        }}
                    >
                        <NavLink
                            className="nav-link active"
                            aria-current="page"
                            to="/user"
                        >
                            {loggedInUsername ? loggedInUsername : 'Sign In'}
                        </NavLink>
                    </li>
                </ul>
                <div className="block md:hidden">
                    <button onClick={handleNav}>
                        {nav ? (
                            <AiOutlineClose size={20} />
                        ) : (
                            <AiOutlineMenu size={20} />
                        )}
                    </button>
                </div>
            </div>
            <div
                className={`fixed top-0 left-0 w-full h-full bg-black bg-opacity-90 z-50 overflow-hidden transition-transform duration-300 ${
                    nav ? 'translate-x-0' : '-translate-x-full'
                }`}
            >
                <div className="max-w-[1200px] mx-auto h-full flex flex-col items-center justify-center">
                    <h1 className="text-3xl font-bold text-red">HOME</h1>
                    <ul className="mt-8 space-y-4 text-2xl">
                        <li>
                            <a href="/rants" className="text-white">
                                Rants
                            </a>
                        </li>
                        <li>
                            <a href="/rantCategory" className="text-white">
                                Categories
                            </a>
                        </li>
                        <li>
                            <a href="/user" className="text-white">
                                Login
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Navibar
