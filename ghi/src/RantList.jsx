import { useState, useEffect } from 'react'
import { Card, Button } from 'flowbite-react'
// import { HiAdjustments, HiCloudDownload, HiUserCircle } from 'react-icons/hi'
import useToken from '@galvanize-inc/jwtdown-for-react'

function RantList() {
    const [rants, setRants] = useState([])
    const { fetchWithCookie } = useToken()

    // const [userId, setUserId] = useState(null)
    const [rantUsers, setRantUsersData] = useState(null)
    const baseUrl = import.meta.env.VITE_API_HOST

    const getLoggedUser = async () => {
        const userData = await fetchWithCookie(`${baseUrl}/token/`)
        console.log(userData)
        if (!userData) {
            console.log('Please log in')
            return
        }

        // setUserId(userData.user.id)
    }

    const getRants = async () => {
        const url = `${baseUrl}/rants`
        try {
            const response = await fetch(url)
            if (response.ok) {
                const data = await response.json()
                setRants(data)
            }
        } catch (error) {
            console.error('Error fetching rants:', error)
        }
    }

    const getAuthors = async () => {
        const authorsURL = `${baseUrl}/user/`
        const authorsResponse = await fetch(authorsURL)
        if (authorsResponse.ok) {
            const authorsData = await authorsResponse.json()
            // console.log(authorsData)
            setRantUsersData(authorsData)
            console.log(rantUsers)
        }
    }

    useEffect(() => {
        getRants()
        getAuthors()
        getLoggedUser()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const RantCard = ({ rant }) => {
        const categories = [
            { id: 1, name: 'Pet' },
            { id: 2, name: 'Software' },
            { id: 3, name: 'Hardware' },
            { id: 4, name: 'Movies' },
            { id: 5, name: 'TV' },
            { id: 6, name: 'Music' },
            { id: 7, name: 'Products' },
            { id: 8, name: 'News' },
            { id: 9, name: 'Shenanigans' },
        ]
        const matchingCategory = categories.find(
            (category) => category.id === rant.category_id
        )
        // const author = rantUsers?.find(
        //     (user) => rants.author_id === getAuthors.name
        // )

        // const handleCommentClick = () => {
        //     if (userId) {
        //         // If user is logged in, navigate to the comment page
        //         // Navigate to /rants/:rant_id/comment
        //         // Will NEED : NavLink
        //     } else {
        //         // If user is not logged in, show a message or redirect to login page
        //         // console.log('Please log in to comment.')
        //     }
        // }

        // const handleViewDetailsClick = () => {
        //     // Navigate to /rants/:rant_id
        //     // NAVLINK
        // }

        // const handleUpdateClick = () => {
        //     // Verify user === author
        //     // rant update
        // }
        // // 182, 82, 0, 0 Dark Orange Bronze RGB

        return (
            <Card className="max-w-sm bg-rgb(0,0,0, 0.5)">
                <h5 className="justify-center flex text-2xl font-bold tracking-tight text-white dark:text-red">
                    {rant.title}
                </h5>
                <p className="justify-center flex font-normal text-gray-700 dark:text-gray-400">
                    Category: {matchingCategory?.name}
                </p>
                <p className="text-white dark:text-white">{rant.content}</p>
                <p className="text-white dark:text-white">
                    <strong>Author:</strong> {rant.author_id}
                </p>
                {/* <Link to={`rants/${rant.id}`}> */}
                <Button color="gray">Details</Button>
                {/* </Link> */}
                <Button color="gray">Update</Button>
            </Card>
        )
    }

    return (
        <div className="w-screen h-screen">
            <h1
                className="flex text-4xl justify-center font-semibold mt-4 text-white bg-clip-padding"
                style={{
                    fontSize: '20px',
                    padding: '15px 30px',
                    fontFamily: 'frijole',
                }}
            >
                Look at All These Rants
            </h1>
            <div
                className="grid grid-cols-3 gap-10"
                style={{
                    fontSize: '17px',
                    padding: '15px 30px',
                    fontFamily: 'frijole',
                }}
            >
                {rants.map((rant) => (
                    <RantCard key={rant.id} rant={rant} />
                ))}
            </div>
        </div>
    )
}

export default RantList
