import { NavLink } from 'react-router-dom'
import { useState, useEffect } from 'react'
import useToken from '@galvanize-inc/jwtdown-for-react'

function Nav() {
    const [loggedInUsername, setLoggedInUsername] = useState(null)
    const { token, fetchWithCookie } = useToken()
    const baseUrl = import.meta.env.VITE_API_HOST

    const getUsername = async () => {
        const userData = await fetchWithCookie(`${baseUrl}/token/`)
        if (!userData) {
            console.log('Not currently logged in')
            setLoggedInUsername(null)
            return
        }

        setLoggedInUsername(userData.user.username)
    }

    useEffect(() => {
        getUsername()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token])

    return (
        <header>
            <li>
                <NavLink className="nav-link active" aria-current="page" to="/">
                    Home
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/rants"
                >
                    Rants
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/rants/new"
                >
                    Rants Create
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/user"
                >
                    {loggedInUsername ? loggedInUsername : 'Sign In'}
                </NavLink>
            </li>
            <p>Categories</p>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/1"
                >
                    Pets
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/2"
                >
                    Software
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/3"
                >
                    Hardware
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/4"
                >
                    Movies
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/5"
                >
                    TV
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/6"
                >
                    Music
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/7"
                >
                    Products
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/8"
                >
                    News
                </NavLink>
            </li>
            <li>
                <NavLink
                    className="nav-link active"
                    aria-current="page"
                    to="/category/9"
                >
                    Shenanigans
                </NavLink>
            </li>
        </header>
    )
}

export default Nav
