import useToken from "@galvanize-inc/jwtdown-for-react";
import { useState, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import { Button } from "flowbite-react";

function SignInSignOut() {
    const [loggedIn, setLoggedIn] = useState(false);
    const [currentUser, setCurrentUser] = useState(null);
    const [existingUser, setExistingUser] = useState(false);
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { token, login, logout, fetchWithCookie } = useToken();
    const nav = useNavigate();
    const baseUrl = import.meta.env.VITE_API_HOST

    const getCurrentUser = async () => {
        const userData = await fetchWithCookie(`${baseUrl}/token/`);
        if (!userData) {
            console.log("Not currently logged in");
            setLoggedIn(false);
            return;
        }

        setCurrentUser(userData);
        setLoggedIn(true);
        console.log("Current User is: ", currentUser?.user?.username, "id: ", currentUser?.user?.id);
    }

    const handleSignUp = async (e) => {
        e.preventDefault();
        const createUrl = `${baseUrl}/user`;
        const formData = { username, email, password };

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(createUrl, fetchConfig);

        if (response.ok) {
            login(formData.username, formData.password);
            nav("/rants");
        }
    };

    const handleSignIn = async (e) => {
        e.preventDefault();
        const formData = { username, password };
        const loginResponse = await login(formData.username, formData.password);
        if (!loginResponse) {
            // TODO modal saying incorrect username or password
            console.log("Invalid credentials");
        }
        nav("/rants");
    };

    const handleUpdate = async (e) => {
        e.preventDefault();
        const updateUrl = `${baseUrl}/user/${currentUser.user.id}/`;
        const formData = { username, email, password };

        const fetchConfig = {
            method: "put",
            credentials: "include",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
            },
        };

        const response = await fetch(updateUrl, fetchConfig);

        if (response.ok) {
            nav("/rants");
        }
    }

    const handleSignOut = async (e) => {
        e.preventDefault();
        try {
            await logout();
            nav("/rants");
        } catch (error) {
            console.error("Unable to logout: ", error);
        }
    };

    const toggleForm = () => {
        setExistingUser((prev) => !prev);
    };

    useEffect(() => {
        getCurrentUser();
        if (loggedIn) {
            getCurrentUser();
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [token]);

    return (
        <div className="flex flex-col items-center justify-center min-h-screen">
            <div className="absolute inset-0 flex items-center justify-center bg-neutral-950">
                {!loggedIn ? (
                    <div>
                        <h1 style={{ color: "white" }}>{existingUser ? "Sign In" : "Sign Up"}</h1>
                        <form onSubmit={existingUser ? handleSignIn : handleSignUp}>
                            <div>
                                <input
                                    onChange={(e) => setUsername(e.target.value)}
                                    value={username}
                                    placeholder="Username"
                                    required
                                    type="text"
                                    id="username"
                                    name="username"
                                    className="form-control"
                                    autoFocus
                                    style={{ color: "black" }}
                                />
                                <label htmlFor="username">Username</label>
                            </div>
                            {!existingUser && (
                                <div>
                                    <input
                                        onChange={(e) => setEmail(e.target.value)}
                                        value={email}
                                        placeholder="Email"
                                        required
                                        type="text"
                                        name="email"
                                        id="email"
                                        className="form-control"
                                        style={{ color: "black" }}
                                    />
                                    <label htmlFor="email">Email</label>
                                </div>
                            )}
                            <div>
                                <input
                                    onChange={(e) => setPassword(e.target.value)}
                                    value={password}
                                    placeholder="Password"
                                    required
                                    type="password"
                                    name="password"
                                    id="password"
                                    className="form-control"
                                    style={{ color: "black" }}
                                />
                                <label htmlFor="password">Password</label>
                            </div>
                            <div className="flex flex-wrap gap-2">
                                <Button outline
                                    gradientDuoTone="pinkToOrange"
                                    type="submit"
                                >{existingUser ? "Log In" : "Sign Up"}</Button>
                            </div>
                        </form>
                        <div className="flex flex-wrap gap-2">
                            <Button outline
                                gradientDuoTone="redToYellow"
                                onClick={toggleForm}
                            >{existingUser ? "Need to sign up?" : "Already have an account?"}</Button>
                        </div>
                    </div>
                ) : <div>
                    <h1 style={{ color: "#873F3A" }}>
                        <Link to={`/user/${currentUser.user.username}`}>{currentUser.user.username}</Link></h1>
                    <div>
                        <div className="flex flex-warp gap-2">
                            <Button outline
                                gradientDuoTone="redToYellow"
                                onClick={handleSignOut}
                            >Sign Out</Button>
                        </div>
                        <h1 style={{ color: "white" }}>Update Credentials</h1>
                        <form onSubmit={handleUpdate}>
                            <div>
                                <input
                                    onChange={(e) => setUsername(e.target.value)}
                                    value={username}
                                    placeholder="Ex: 'Tragic Bronson'"
                                    required
                                    type="text"
                                    name="username"
                                    id="username"
                                    className="form-control"
                                    style={{ color: "black" }}
                                />
                                <label htmlFor="username"
                                    style={{ color: "#873F3A" }}
                                >Username</label>
                            </div>
                            <div>
                                <input
                                    onChange={(e) => setEmail(e.target.value)}
                                    value={email}
                                    placeholder="Ex: no@u.com"
                                    required
                                    type="text"
                                    name="email"
                                    id="email"
                                    className="form-control"
                                    style={{ color: "black" }}
                                />
                                <label htmlFor="email"
                                    style={{ color: "#873F3A" }}
                                >Email</label>
                            </div>
                            <div>
                                <input
                                    onChange={(e) => setPassword(e.target.value)}
                                    value={password}
                                    placeholder="Password"
                                    required
                                    type="password"
                                    name="password"
                                    id="password"
                                    className="form-control"
                                    style={{ color: "black" }}
                                />
                                <label htmlFor="email"
                                    style={{ color: "#873F3A" }}
                                >Password</label>
                            </div>
                            <div className="flex flex-wrap gap-2">
                                <Button outline
                                    gradientDuoTone="pinkToOrange"
                                    type="submit"
                                >Update</Button>
                            </div>
                        </form>
                        <h6 style={{ color: "#523e3c" }}>
                            <Link to="/user/delete">Goodbye Forever?</Link>
                        </h6>
                    </div>
                </div>}
            </div>
        </div>
    );
}

export default SignInSignOut;
