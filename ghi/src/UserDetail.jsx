import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { AiFillLike, AiFillDislike } from 'react-icons/ai';
import { Button, Card } from 'flowbite-react';

function UserDetail() {
    const { username } = useParams();
    const [viewRants, setViewRants] = useState(false);
    const [rants, setRants] = useState([]);
    const [comments, setComments] = useState([]);
    const baseUrl = import.meta.env.VITE_API_HOST

    const fetchRants = async () => {
        if (username) {
            const rantsUrl = `${baseUrl}/user/${username}/rants/`;
            const rantsResponse = await fetch(rantsUrl);
            if (rantsResponse.ok) {
                const rantsData = await rantsResponse.json();

                const rantsWithLikes = await Promise.all(
                    rantsData.map(async (rant) => {
                        const likeResponse = await fetch(`${baseUrl}/likes/?rant_id=${rant.id}`);
                        const dislikeResponse = await fetch(`${baseUrl}/dislikes/?rant_id=${rant.id}`);
                        if (likeResponse.ok && dislikeResponse.ok) {
                            const likeData = await likeResponse.json();
                            const dislikeData = await dislikeResponse.json();
                            const spice = likeData - dislikeData;
                            return {
                                ...rant,
                                likes: spice
                            };
                        } else {
                            console.error("Unable to count likes ", likeResponse.statusText);
                            return { ...rant, likes: 0 };
                        }
                    })
                );

                setRants(rantsWithLikes.sort((a, b) =>
                    new Date(b.date_created) - new Date(a.date_created)));
            }
        }
    }

    const fetchComments = async () => {
        if (username) {
            const commentsUrl = `${baseUrl}/user/${username}/comments/`;
            const commentsResponse = await fetch(commentsUrl);
            if (commentsResponse.ok) {
                const commentsData = await commentsResponse.json();

                const commentsWithLikes = await Promise.all(
                    commentsData.map(async (comment) => {
                        const likeResponse = await fetch(`${baseUrl}/comment_likes/?comment_id=${comment.id}`);
                        const dislikeResponse = await fetch(`${baseUrl}/comment_dislikes/?comment_id=${comment.id}`);
                        if (likeResponse.ok && dislikeResponse.ok) {
                            const likeData = await likeResponse.json();
                            const dislikeData = await dislikeResponse.json();
                            const spice = likeData - dislikeData;
                            return {
                                ...comment,
                                likes: spice
                            };
                        } else {
                            console.error("Unable to count likes ", likeResponse.statusText);
                            return { ...comment, likes: 0 };
                        }
                    })
                );

                setComments(commentsWithLikes.sort((a, b) =>
                    new Date(b.date_created) - new Date(a.date_created)));
            }
        }
    }

    const iceOrSpice = () => {
        const total = rants.reduce((acc, rant) => acc + rant.likes, 0) +
            comments.reduce((acc, comment) => acc + comment.likes, 0);
        return total;
    }

    const toggleView = () => {
        setViewRants((prev) => !prev);
        console.log(comments);
        console.log(rants);
    };

    useEffect(() => {
        fetchRants();
        fetchComments();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    return (
        <div className="flex flex-col min-h-screen">
            <div className="absolute inset-0 flex flex-col justify-start p-4 bg-neutral-950" style={{ top: "100px" }}>
                <h1 style={{ color: "white" }}>{username}</h1>
                <p>Spicyness: {iceOrSpice()}</p>
                <div className="flex flex-wrap gap-2">
                    <Button gradientDuoTone="pinkToOrange"
                        onClick={toggleView}
                    >{viewRants ? "Rants" : "Smol Rants"}
                    </Button>
                </div>
                {!viewRants ? (
                    <div>
                        <h1 style={{ color: "#873F3A" }}>Past Rants</h1>
                        {rants.map(rant => (
                            <Card key={rant.id} className="w-full" style={{
                                width: "100%",
                                backgroundColor: "rgba(43,48,48,0.9)",
                                border: "#873F3A"
                            }}><Link to={`/rants/${rant.id}`}>
                                    <h5 className="text-2xl font-bold">
                                        {rant.title}
                                    </h5>
                                    <p className="font-normal"
                                        style={{ display: "flex", alignItems: "center" }}
                                    >
                                        <AiFillLike
                                            color="red"
                                        />
                                        {rant.likes}
                                        <AiFillDislike
                                            color="blue"
                                        />
                                    </p>
                                    {rant.date_created}
                                </Link>
                            </Card>
                        ))}
                    </div>
                ) : <div>
                    <h1 style={{ color: "#873F3A" }}>Past Rants on Rants</h1>
                    {comments.map(comment => (
                        <Card key={comment.id} className="w-full" style={{
                            width: "100%",
                            backgroundColor: "rgba(43,48,48,0.9)",
                            border: "#873F3A"
                        }} ><Link to={`/rants/${comment.rant_id}`}>
                                <h5 className="text-xl font-bold">
                                    {comment.content}
                                </h5>
                                <p className="font-normal"
                                    style={{ display: "flex", alignItems: "center" }}
                                >
                                    <AiFillLike
                                        color="red"
                                    />
                                    {comment.likes}
                                    <AiFillDislike
                                        color="blue"
                                    />
                                </p>
                            </Link>
                        </Card>
                    ))}
                </div>
                }
            </div>
        </div>
    );
}

export default UserDetail;
