import { useState, useEffect } from 'react'
import './App.css'
import { useParams, Link } from 'react-router-dom'

function CategoryRant() {
    const { category_id } = useParams()
    const [cat, setCat] = useState(null)
    const [rants, setRants] = useState(null)
    const baseUrl = import.meta.env.VITE_API_HOST

    const getCat = async () => {
        const url = `${baseUrl}/category/${category_id}`
        try {
            const response = await fetch(url)
            console.log(response)
            if (response.ok) {
                const catData = await response.json()
                setCat(catData)
            } else {
                console.error(
                    'Error fetching category',
                    response.status,
                    response.statusText
                )
            }
        } catch (error) {
            console.error('Error fetching category', error)
        }
    }

    const getRants = async () => {
        const rantURL = `${baseUrl}/rants`
        try {
            const response = await fetch(rantURL)
            if (response.ok) {
                const rantData = await response.json()
                setRants(rantData)
            }
        } catch (error) {
            console.error('Error fetching rants:', error)
        }
    }

    const [rantUsers, setRantUsersData] = useState(null)
    const getAuthors = async () => {
        const authorsURL = `${baseUrl}/user/`
        const authorsResponse = await fetch(authorsURL)
        if (authorsResponse.ok) {
            const authorsData = await authorsResponse.json()
            // console.log(authorsData)
            setRantUsersData(authorsData)
        }
    }

    useEffect(() => {
        getCat()
        getRants()
        getAuthors()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [category_id])

    if (!cat || !rants) {
        return <p>Loading rants...</p>
    }

    return (
        <div className="flex flex-col items-center justify-center min-h-screen">
            <div className="absolute inset-0 flex items-center justify-center bg-black">
                <h1
                    className="header flex justify-center text-white"
                    style={{
                        fontSize: '40px',
                        padding: '15px 30px',
                        fontFamily: 'frijole',
                    }}
                >
                    {cat.name?.toUpperCase()} RANTS!
                </h1>
                <table className="table justify-center">
                    <thead>
                        <tr className="table-success text-white">
                            <th
                                style={{
                                    fontSize: '25px',
                                    padding: '15px 30px',
                                    fontFamily: 'frijole',
                                }}
                            >
                                Title
                            </th>
                            <th
                                style={{
                                    fontSize: '25px',
                                    padding: '15px 30px',
                                    fontFamily: 'frijole',
                                }}
                            >
                                Author
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {rants
                            .filter((rant) => rant.category_id === +category_id)
                            .map((rant) => {
                                const matchingAuthor =
                                    rantUsers !== null
                                        ? rantUsers.find(
                                              (rantUser) =>
                                                  rantUser.id === rant.author_id
                                          )
                                        : null
                                return (
                                    <tr key={rant.id}>
                                        <td
                                            className="row text-white"
                                            style={{
                                                fontSize: '20px',
                                                padding: '15px 30px',
                                                fontFamily: 'frijole',
                                            }}
                                        >
                                            <Link to={`/rants/${rant.id}`}>
                                                {rant.title}
                                            </Link>
                                        </td>
                                        <td
                                            className="row text-white"
                                            style={{
                                                fontSize: '20px',
                                                padding: '15px 30px',
                                                fontFamily: 'frijole',
                                            }}
                                        >
                                            <Link
                                                to={
                                                    matchingAuthor
                                                        ? `/user/${matchingAuthor.username}`
                                                        : null
                                                }
                                            >
                                                {matchingAuthor
                                                    ? matchingAuthor.username
                                                    : null}
                                            </Link>
                                        </td>
                                    </tr>
                                )
                            })}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default CategoryRant
