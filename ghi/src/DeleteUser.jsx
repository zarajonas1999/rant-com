import useToken from '@galvanize-inc/jwtdown-for-react'
import { useNavigate } from 'react-router-dom'
import { Button } from 'flowbite-react'
import anime from 'animejs'

const DeleteUser = () => {
    const { logout, fetchWithCookie } = useToken()
    const nav = useNavigate()
    const baseUrl = import.meta.env.VITE_API_HOST

    const handleDelete = async () => {
        const data = await fetchWithCookie(`${baseUrl}/token/`)
        if (!data) {
            console.log('Not currently logged in')
            return
        }

        const url = `${baseUrl}/user/${data.user.id}/`

        try {
            const response = await fetch(url, {
                method: 'delete',
                credentials: 'include',
            })

            if (response.ok) {
                console.log('Bye bye!')
                logout()
                nav('/')
            } else {
                console.log('Unable to delete user.')
            }
        } catch (error) {
            console.error("Unable to delete user: ", error);
        }

    };

    const handleHover = () => {
        animateButton()
    }

    const animateButton = () => {
        const button = document.getElementById('delete-btn')
        const windowWidth = window.innerWidth
        const windowHeight = window.innerHeight

        let top = getRandomNumber(windowHeight - button.offsetHeight, 0.3, 0.7)
        let left = getRandomNumber(windowWidth - button.offsetWidth, 0.3, 0.7)

        // stop button from moving outside of window
        top = Math.min(Math.max(0, top), windowHeight - button.offsetHeight)
        left = Math.min(Math.max(0, left), windowWidth - button.offsetWidth)

        anime({
            targets: button,
            left: `${left}px`,
            top: `${top}px`,
            easing: 'easeOutCirc',
        })
    }

    const getRandomNumber = (num, minPercent = 0.3, maxPercent = 0.7) => {
        const minValue = num * minPercent
        const maxValue = num * maxPercent
        return Math.floor(Math.random() * (maxValue - minValue + 1))
    }

    return (
        <div className="flex flex-col items-center justify-center min-h-screen">
            <div className="absolute inset-0 flex items-center justify-center bg-neutral-950">
                <div className="flex flex-wrap gap-2">
                    <Button
                        id="delete-btn"
                        pill
                        color="failure"
                        onClick={handleDelete}
                        onMouseOver={handleHover}
                    >
                        Delete User
                    </Button>
                </div>
            </div>
        </div>
    )
}

export default DeleteUser
