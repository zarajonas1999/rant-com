import { useState, useEffect } from 'react';
import { AiFillLike, AiFillDislike } from 'react-icons/ai';
import useToken from "@galvanize-inc/jwtdown-for-react";
import { Link } from 'react-router-dom';
import { Card } from 'flowbite-react';

function MainPage() {
    const [loggedIn, setLoggedIn] = useState(false);
    const [currentUser, setCurrentUser] = useState(null);
    const [rants, setRants] = useState([]);
    const { fetchWithCookie } = useToken();
    const [currentPage, setCurrentPage] = useState(1);
    const [rantsPerPage] = useState(10);
    const baseUrl = import.meta.env.VITE_API_HOST;

    const getCurrentUser = async () => {
        const userData = await fetchWithCookie(`${baseUrl}/token/`);
        if (!userData) {
            console.log("Not currently logged in");
            setLoggedIn(false);
            return;
        }

        setCurrentUser(userData);
        setLoggedIn(true);
        console.log("getCurrentUser ran");
    }

    const loadMain = async () => {
        const response = await fetch(`${baseUrl}/rants/`);
        if (response.ok) {
            const data = await response.json();

            const rantsWithLikes = await Promise.all(
                data.map(async (rant) => {
                    const authorResponse = await fetch(`${baseUrl}/user/${rant.author_id}`);
                    const likeResponse = await fetch(`${baseUrl}/likes/?rant_id=${rant.id}`);
                    const dislikeResponse = await fetch(`${baseUrl}/dislikes/?rant_id=${rant.id}`);
                    if (likeResponse.ok && dislikeResponse.ok && authorResponse.ok) {
                        const likeData = await likeResponse.json();
                        const dislikeData = await dislikeResponse.json();
                        const authorData = await authorResponse.json();
                        const spice = likeData - dislikeData;
                        return {
                            ...rant,
                            likes: spice,
                            author: authorData.username,
                        };
                    } else {
                        console.error("Unable to count likes ", likeResponse.statusText);
                        return {
                            ...rant,
                            likes: 0,
                            author: "unknown",
                        };
                    }
                })
            );

            setRants(rantsWithLikes.sort((a, b) =>
                new Date(b.date_created) - new Date(a.date_created)));
            getCurrentUser();
        }
    }

    const handleRantLike = async (rant) => {
        try {
            if (loggedIn) {
                const response = await fetch(
                    `${baseUrl}/likes?user_id=${currentUser.user.id}&rant_id=${rant.id}`,
                    {
                        method: "post",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        credentials: "include",
                    });

                if (response.ok) {
                    loadMain();
                } else {
                    console.error("unable to like ", response.statustext);
                }
            }
        } catch (error) {
            console.error("error: ", error);
        }
    }

    const handleRantDislike = async (rant) => {
        try {
            if (loggedIn) {
                const response = await fetch(
                    `${baseUrl}/dislikes?user_id=${currentUser.user.id}&rant_id=${rant.id}`,
                    {
                        method: "post",
                        headers: {
                            "Content-Type": "application/json",
                        },
                        credentials: "include",
                    });

                if (response.ok) {
                    loadMain();
                } else {
                    console.error("unable to dislike ", response.statustext);
                }
            }
        } catch (error) {
            console.error("error: ", error);
        }

    }

    useEffect(() => {
        loadMain();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [loggedIn, currentPage]);

    const indexOfLast = currentPage * rantsPerPage;
    const indexOfFirst = indexOfLast - rantsPerPage;
    const currentRants = rants.slice(indexOfFirst, indexOfLast);

    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    return (
        <div className="container">
            <div className="flex flex-col min-h-screen">
                <div className="absolute inset-0 flex flex-col justify-start p-4 bg-neutral-950" style={{ top: "100px" }}>
                    {currentRants.map(rant => (
                        <Card key={rant.id}
                            className="my-3 full-width-card"
                            style={{
                                backgroundColor: "rgba(43,48,48,0.9)",
                                border: "#873F3A"
                            }}>
                            <div className="d-flex justify-content-between align-items-center p-3">
                                <div>
                                    <h5 className="text-2xl font-bold"><Link to={`/rants/${rant.id}`}>{rant.title}</Link></h5>
                                </div>
                                <div className="font-normal"
                                    style={{ display: "flex", alignItems: "center" }}
                                >
                                    <AiFillLike color="red" onClick={() => handleRantLike(rant)} style={{ cursor: "pointer" }} />
                                    <span className="mx-1">{rant.likes}</span>
                                    <AiFillDislike color="blue" onClick={() => handleRantDislike(rant)} style={{ cursor: "pointer" }} />
                                </div>
                                <p className="text-muted mb-0">by: {rant.author}</p>
                            </div>
                            <div className="p-3 text-muted">{rant.date_created}</div>
                        </Card>
                    ))}
                    <p className="lead mb-4">
                        The premiere solution for all your ranting needs!
                    </p>
                    <nav className="d-flex justify-content-between align-items-center p-3">
                        <ul className="pagination">
                            {currentPage > 1 && (
                                <li className="page-item">
                                    <button className="page-link" onClick={() => paginate(currentPage - 1)}>
                                        Previous
                                    </button>
                                </li>
                            )}
                            {currentPage < Math.ceil(rants.length / rantsPerPage) && (
                                <li className="page-item">
                                    <button className="page-link" onClick={() => paginate(currentPage + 1)}>
                                        Next
                                    </button>
                                </li>
                            )}
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    );
}

export default MainPage;
