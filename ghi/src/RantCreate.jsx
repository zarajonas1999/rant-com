import { useState, useEffect } from 'react'
import './App.css'
import useToken from '@galvanize-inc/jwtdown-for-react'
import { Button } from 'flowbite-react'

function RantForm() {
    const { fetchWithCookie } = useToken()
    const [title, setTitle] = useState('')
    const [category, setCategory] = useState('')
    const [content, setContent] = useState('')
    const baseUrl = import.meta.env.VITE_API_HOST

    const [userID, setUserID] = useState(null)
    const getLoggedUser = async () => {
        const userData = await fetchWithCookie(`${baseUrl}/token/`)
        if (!userData) {
            console.log('Please log in')
            return
        }

        setUserID(userData.user.id)
    }

    // console.log(userID)

    const categories = [
        { id: 1, name: 'Pet' },
        { id: 2, name: 'Software' },
        { id: 3, name: 'Hardware' },
        { id: 4, name: 'Movies' },
        { id: 5, name: 'TV' },
        { id: 6, name: 'Music' },
        { id: 7, name: 'Products' },
        { id: 8, name: 'News' },
        { id: 9, name: 'Shenanigans' },
    ]

    useEffect(() => {
        getLoggedUser()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleTitleChange = (event) => {
        setTitle(event.target.value)
    }

    const handleCategoryChange = (event) => {
        setCategory(event.target.value)
    }

    const handleContentChange = (event) => {
        setContent(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {
            title: title,
            category_id: category,
            content: content,
            author_id: userID,
        }

        try {
            const response = await fetch(`${baseUrl}/rants/new`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(data),
            })
            if (response.ok) {
                setTitle('')
                setCategory('')
                setContent('')
            }
        } catch (error) {
            console.error('Error:', error)
        }
    }

    return (
        <div className="flex flex-col items-center justify-center min-h-screen">
            <div className="absolute inset-0 flex items-center justify-center bg-neutral-950">
                <div className="col justify-content-center">
                    <div
                        className="shadow p-4 mt-4"
                        style={{ fontFamily: 'Oswald' }}
                    >
                        <h1> Rant On </h1>

                        <form onSubmit={handleSubmit}>
                            <div className="mb-4">
                                <input
                                    value={title}
                                    onChange={handleTitleChange}
                                    placeholder="Title"
                                    required
                                    type="text"
                                    name="title"
                                    id="title"
                                    className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-400"
                                />
                            </div>

                            <div className="mb-4">
                                <select
                                    value={category}
                                    onChange={handleCategoryChange}
                                    name="category"
                                    id="category"
                                    className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-400"
                                >
                                    <option>What is this about?!</option>
                                    {categories.map((category) => (
                                        <option
                                            key={category.id}
                                            value={category.id}
                                        >
                                            {category.name}
                                        </option>
                                    ))}
                                </select>
                            </div>

                            <div className="mb-4">
                                <textarea
                                    value={content}
                                    onChange={handleContentChange}
                                    placeholder="Content"
                                    name="content"
                                    id="content"
                                    required
                                    rows="4"
                                    className="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-400"
                                ></textarea>
                            </div>
                            <Button color="failure" size="lg" type="submit">
                                {' '}
                                Create Rant
                            </Button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default RantForm
