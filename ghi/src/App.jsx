import { BrowserRouter, Routes, Route } from 'react-router-dom'
import './App.css'
import CategoryRant from './CategoryRant'
// import Nav from './Nav'
import DeleteUser from './DeleteUser'
import UserDetail from './UserDetail'
// import MainPage from './MainPage'
import RantList from './RantList'
import RantForm from './RantCreate'
import RantUpdate from './RantUpdate'
import RantDetails from './RantDetails'
import SignInSignOut from './SignInSignOut'
import LandingPage from './LandingPage'
import Footer from './Components/Footer'
import Navibar from './Components/Navibar'

function App() {
    return (
        <BrowserRouter>
            <Navibar />
            <div className="flex">
                <Routes>
                    <Route path="/" element={<LandingPage />} />
                    <Route path="user">
                        <Route index element={<SignInSignOut />} />
                        <Route path="delete" element={<DeleteUser />} />
                        <Route path=":username" element={<UserDetail />} />
                    </Route>
                    <Route path="/rants" element={<RantList />} />
                    <Route path="/rants/new" element={<RantForm />} />
                    <Route
                        path="/rants/:rants_id/review"
                        element={<RantUpdate />}
                    />
                    <Route path="/rants/:rants_id" element={<RantDetails />} />
                    <Route
                        path="/category/:category_id"
                        element={<CategoryRant />}
                    />
                </Routes>
            </div>
            <Footer />
        </BrowserRouter>
    )
}

export default App
